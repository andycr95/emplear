<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmpleosTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'empleos';

    /**
     * Run the migrations.
     * @table empleos
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('titulo', 45)->nullable()->default(null);
            $table->string('nombre', 45)->nullable()->default(null);
            $table->string('empresa', 45)->nullable()->default(null);
            $table->integer('telefono')->nullable()->default(null);
            $table->string('ocupacion', 99)->nullable()->default(null);
            $table->date('fecha_inicio')->nullable()->default(null);
            $table->date('fecha_fin')->nullable()->default(null);
            $table->unsignedInteger('users_id');
            $table->unsignedInteger('ciudad_id');
            $table->unsignedInteger('tipoempleo_id');
            $table->unsignedInteger('categoria_id');
            $table->string('estado', 45)->nullable()->default(null);

            $table->index(["users_id"], 'empleos_users_id_foreign');

            $table->index(["categoria_id"], 'empleos_categoria_id_foreign');

            $table->index(["tipoempleo_id"], 'empleos_tipoempleo_id_foreign');

            $table->index(["ciudad_id"], 'empleos_ciudad_id_foreign');
            $table->timestamps();


            $table->foreign('categoria_id', 'empleos_categoria_id_foreign')
                ->references('id')->on('categoria')
                ->onDelete('restrict')
                ->onUpdate('restrict');

            $table->foreign('ciudad_id', 'empleos_ciudad_id_foreign')
                ->references('id')->on('ciudad')
                ->onDelete('restrict')
                ->onUpdate('restrict');

            $table->foreign('tipoempleo_id', 'empleos_tipoempleo_id_foreign')
                ->references('id')->on('tipoempleo')
                ->onDelete('restrict')
                ->onUpdate('restrict');

            $table->foreign('users_id', 'empleos_users_id_foreign')
                ->references('id')->on('users')
                ->onDelete('restrict')
                ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
