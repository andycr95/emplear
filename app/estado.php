<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class estado extends Model
{
    protected $table = 'estado';
    protected $fillable = ['nombre', 'estado'];

    public function estudios()
    {
        return $this->hasMany(estudios::class);
    }

}
