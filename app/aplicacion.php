<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\users;
use App\vacantes;
use App\datosp;
use App\aplicantes;


class aplicacion extends Model
{
    protected $table = 'aplicacion';
    protected $fillable = ['users_id','vacantes_id','empresa_id','estado'];


    public function users()
    {
        return $this->belongsTo(users::class);
    }

    public function aplicante()
    {
        return $this->hasOne(aplicantes::class);
    }

    public function datosp()
    {
        return $this->belongsToMany(datosp::class);
    }

    public function vacantes()
    {
        return $this->belongsToMany(vacantes::class);
    }
   
    public function vacan()
    {
        return $this->belongsTo(vacantes::class);
    }
   


}
