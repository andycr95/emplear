@extends('empresa.principal')


@section('header')
<div class="container page-name">
    <h1 class="text-center">Aplicantes</h1>
    <p class="lead text-center">Estos son los candidatos que que han aplicado a tus ofertas</p>
    <br><br><br>
</div>
@stop
@section('content')
    <section class="no-padding-top bg-alt">
        <div class="container">
            <div class="row">

            <div class="col-xs-12">
                <div class="row grid-jobs">
                    @foreach($aplicacion as $aplicacion)
                        <div class="col-md-4 grid-item job">
                            <a class="item-block" href="/candidato/perfil/{{$aplicacion->users->id}}">
                                <header>
                                    <img src="{{$aplicacion->users->datosp->foto}}" alt="">
                                    <div class="hgroup">
                                        <h4>{{$aplicacion->users->name}} {{$aplicacion->users->apellidos}}</h4>
                                        <h5>{{$aplicacion->users->datosp->areas->nombre}}</h5>
                                        <span class="label label-success"></span>
                                    </div>
                                </header>
                                <div class="item-body">
                                    <ul class="details">
                                        <li><i class="fa fa-map-marker"></i><span>{{$aplicacion->users->datosp->ciudad->nombre}} - {{$aplicacion->users->datosp->ciudad->departamento->nombre}}</span></li>
                                        <li><i class="fa fa-email"></i><span>{{$aplicacion->users->email}}</span></li>
                                        <li><i class="fa fa-phone"></i><span>{{$aplicacion->users->datosp->licencia->nombre}} - {{$aplicacion->users->datosp->numero_licencia}}</span></li>
                                    </ul>
                                </div>
                            </a>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
        </div>
    </section>
@stop
