<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAplicantesTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'aplicantes';

    /**
     * Run the migrations.
     * @table aplicantes
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('users_id');
            $table->string('estado', 45)->nullable()->default(null);
            $table->unsignedInteger('empresa_id');
            $table->unsignedInteger('aplicacion_id');

            $table->index(["estado"], 'aplicantes_vacantes_id_foreign');

            $table->index(["aplicacion_id"], 'aplicantes_aplicacion_id_foreign');

            $table->index(["empresa_id"], 'aplicantes_empresa_id_foreign');

            $table->index(["users_id"], 'contenido_users_id_foreign');
            $table->nullableTimestamps();


            $table->foreign('aplicacion_id', 'aplicantes_aplicacion_id_foreign')
                ->references('id')->on('aplicacion')
                ->onDelete('restrict')
                ->onUpdate('restrict');

            $table->foreign('empresa_id', 'aplicantes_empresa_id_foreign')
                ->references('id')->on('empresa')
                ->onDelete('restrict')
                ->onUpdate('restrict');

            $table->foreign('users_id', 'contenido_users_id_foreign')
                ->references('id')->on('users')
                ->onDelete('restrict')
                ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
