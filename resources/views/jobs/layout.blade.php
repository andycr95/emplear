
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Emplear</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Busca y/o publica ofertas laborales para desarrolladores php y Laravel.">
    <meta property="og:title" content="Emplear">
    <meta property="og:description" content="Busca y/o publica ofertas laborales.">
    <meta property="og:image" content="img/header-bg.jpg">
    <meta name="twitter:title" content="Empleo Laravel">
    <meta name="twitter:description" content="Busca y/o publica ofertas laborales para desarrolladores php y Laravel.">
    <meta name="twitter:image" content="https://empleolaravel.com/img/bg-banner1.jpg">
    <meta name="twitter:card" content="summary_large_image">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link href="/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <link rel="stylesheet" href="/css/simplemde.min.css">
    <link href='https://fonts.googleapis.com/css?family=Oswald:100,300,400,500,600,800%7COpen+Sans:300,400,500,600,700,800%7CMontserrat:400,700' rel='stylesheet' type='text/css'>
    <link rel="icon" href="/img/favicon.ico">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <link href="/css/app2.css" rel="stylesheet">
     <link href='https://fonts.googleapis.com/css?family=Oswald:100,300,400,500,600,800%7COpen+Sans:300,400,500,600,700,800%7CMontserrat:400,700' rel='stylesheet' type='text/css'>
</head>

<body class="nav-on-header smart-nav" id="app">
<nav class="navbar">
    <div class="container">
        <div class="pull-left">
            <a class="navbar-toggle" href="#" data-toggle="offcanvas"><i class="ti-menu"></i></a>
            <div class="logo-wrapper">
                <a class="logo" href="/"><img src="img/logo3.png" alt="Empleo Laravel logo"></a>
                <a class="logo-alt" href="/"><img src="img/logo4.png" alt="Empleo Laravel JOBS logo"></a>
            </div>
        </div>
        <div class="pull-right user-login">
            <a class="btn btn-sm btn-primary" href="https://empleolaravel.com/login">Login y registro</a>
        </div>
        <ul class="nav-menu">
            <li><a href="https://empleolaravel.com">Home</a></li>
            <li><a href="https://empleolaravel.com/jobs">Ofertas laborales</a></li>
            <li><a href="https://empleolaravel.com/developers">Desarrolladores</a></li>
            <li><a href="https://empleolaravel.com/terminos-y-condiciones">Terminos y condiciones</a></li>
            <li><a href="https://empleolaravel.com/politica-de-privacidad">Política de privacidad</a></li>
        </ul>
    </div>
</nav>
<header style="background-image: url(/images/header-bg.jpg)" class="page-header bg-img">
    
</header>
<main>

    @yield('jobs')

</main>

<footer class="site-footer">
    <!-- Top section -->
    <div class="container">
        <div class="row" id="about">
            <div class="col-sm-12 col-md-6">
                <div class="single-footer-widget mail-chimp">
                    <h6 class="mb-20">Instragram Feed</h6>
                    <ul class="instafeed d-flex flex-wrap">
                        <li><img src="/img/i1.jpg" alt=""></li>
                    </ul>
                </div>
            </div>

            <div class="col-xs-6 col-md-3">
                <h6>Top Products</h6>
                <ul class="footer-nav">
                    <li><a href="#">Managed Website</a></li>
                    <li><a href="#">Manage Reputation</a></li>
                    <li><a href="#">Power Tools</a></li>
                    <li><a href="#">Marketing Service</a></li>
                </ul>
            </div>
            <div class="col-xs-6 col-md-3">
                <h6>Agradecimientos</h6>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
            </div>
        </div>

        <hr>
    </div>
    <!-- END Top section -->

    <!-- Bottom section -->
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-sm-6 col-xs-12">
                <p class="copyright-text">Derechos reservados &copy; 2018 por <a href="/" target="_blank">Emplear</a>.</p>
            </div>

            <div class="col-md-4 col-sm-6 col-xs-12">
                <ul class="social-icons">
                    <li><a class="twitter" href="https://twitter.com/laraveles" target="_blank"><i class="fa fa-twitter"></i></a></li>
                </ul>
            </div>
        </div>
    </div>
    <!-- END Bottom section -->
    <form method="POST" action="https://empleolaravel.com" accept-charset="UTF-8" id="form-delete"><input name="_method" type="hidden" value="DELETE"><input name="_token" type="hidden" value="JkXa4dJUwvaXI2zg76c9nwzNoBty2fHh0B4ESJwg">
    </form>
</footer>
<script src="AdminLTE/plugins/jquery/jquery.min.js"></script>
<script src="AdminLTE/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="AdminLTE/dist/js/adminlte.min.js"></script>
<script src="AdminLTE/dist/js/demo.js"></script>
<script src="https://cdn.jsdelivr.net/simplemde/latest/simplemde.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/plupload/2.2.1/plupload.full.min.js"></script>
<script src="https://empleolaravel.com/js/app.js"></script>
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-58752295f8a04956"></script>
<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-31272861-12', 'auto');
    ga('send', 'pageview');

</script></body>
</html>
