<section>
    <div class="container">
        <div class="row item-blocks-connected">
                <header class="section-header">
                    <span>Últimas ofertas</span>
                    <h2>Últimas ofertas</h2>
                </header>
             <!-- Job item -->
            @foreach($vacantes as $vacantes)    
                <div class="col-xs-12">
                    <a class="item-block" href="/job/{{ $vacantes->id }}" >        
                        <header>
                            <img src="{{$vacantes->empresa->logo}}" alt="">
                            <div class="hgroup">
                            <h4>{{$vacantes->nombre}}</h4>
                            <span class="fa fa-clock-o"> hace {{($vacantes->created_at)->format('d')}} días</span>
                            </div>
                            <div class="header-meta">
                                <span class="location">{{$vacantes->ciudad->nombre}}-{{$vacantes->ciudad->departamento->nombre}}</span>
                                @if($vacantes->tipoempleo->id === 1)
                                <span class="label label-success">{{ $vacantes->tipoempleo->nombre }}</span>
                                @elseif($vacantes->tipoempleo->id === 2)
                                <span class="label label-info">{{ $vacantes->tipoempleo->nombre }}</span> 
                                @elseif($vacantes->tipoempleo->id === 3)
                                <span class="label label-warning">{{ $vacantes->tipoempleo->nombre }}</span>
                                @elseif($vacantes->tipoempleo->id === 4)
                                <span class="label label-danger">{{ $vacantes->tipoempleo->nombre }}</span>
                                @endif
                            </div>
                        </header>
                    </a>
                </div>
            @endforeach
        </div>
        <br><br>
        <p class="text-center"><a class="btn btn-info" href="/jobs">Ver todas las ofertas</a></p>
    </div>
</section>