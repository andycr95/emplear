<div class="container">
    <div class="col-xs-12">    
            <br><br>
            <h2>Ofrecemos <mark>contacto</mark> entre las compañías y empleados</h2>
            <h5 class="font-alt">Encuentra el trabajo que deseas!</h5>
        <br><br><br>
        <form method="GET" action="/jobb" accept-charset="UTF-8" class="header-job-search">
            <div class="input-keyword">
                <input class="form-control" name="nombre" value="">
            </div> 
            <div class="input-location">
                <select class="form-control" name="buscar" id="">
                    <option name="buscar" value="">Seleccione una ciudad</option>
                @foreach ($ciudad as $ciudad)
                    <option name="buscar" value="{{ $ciudad->id }}">{{ $ciudad->nombre }}</option>  
                @endforeach
                </select>
            </div>
            <div class="btn-search">
                <button class="btn btn-primary" type="submit">Buscar</button>
            </div>
        </form>
    </div>
</div>