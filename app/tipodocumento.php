<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class tipodocumento extends Model
{
    protected $table = 'tipodocumento';
    protected $fillable = ['nombre', 'estado'];

    public function datosp()
    {
        return $this->hasMany(datosp::class);
    }
}
