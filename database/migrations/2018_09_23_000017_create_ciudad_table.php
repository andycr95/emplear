<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCiudadTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'ciudad';

    /**
     * Run the migrations.
     * @table ciudad
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('codigo');
            $table->string('nombre', 45)->nullable()->default(null);
            $table->unsignedInteger('departamento_id');
            $table->string('estado', 45)->nullable()->default(null);

            $table->index(["departamento_id"], 'ciudad_departamento_id_foreign');

            $table->unique(["codigo"], 'ciudad_codigo_unique');


            $table->foreign('departamento_id', 'ciudad_departamento_id_foreign')
                ->references('id')->on('departamento')
                ->onDelete('restrict')
                ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
