@extends('empresa.principal')


@section('header')

<header class="site-header size-lg text-center" style="background-image: url(/img/candidatos.jpg)">
    <div class="container page-name">
        <h1 class="text-center">Publicaciones</h1>
        <p class="lead text-center">Estos son las publicaciones has realizado</p>
    </div>
</header>
@stop
@section('content')
    <section class="no-padding-top bg-alt">
        <div class="container">
            <div class="row">

                <div class="col-xs-12">
                    <div class="row grid-jobs">
                        @foreach($publicaciones as $publicaciones)
                            <div class="col-md-4 grid-item job">
                                <a class="item-block" href="/job/{{$publicaciones->id}}">
                                    <header>
                                        <img src="{{$publicaciones->empresa_id}}" alt="">
                                        <div class="hgroup">
                                            <h4>{{$publicaciones->nombre}}</h4> 
                                            @if($publicaciones->tipoempleo->id === 1)
                                            <span class="label label-success">{{ $publicaciones->tipoempleo->nombre }}</span>
                                            @elseif($publicaciones->tipoempleo->id === 2)
                                            <span class="label label-info">{{ $publicaciones->tipoempleo->nombre }}</span> 
                                            @elseif($publicaciones->tipoempleo->id === 3)
                                            <span class="label label-warning">{{ $publicaciones->tipoempleo->nombre }}</span>
                                            @elseif($publicaciones->tipoempleo->id === 4)
                                            <span class="label label-danger">{{ $publicaciones->tipoempleo->nombre }}</span>
                                            @endif  
                                        </div>
                                    </header>
                                    <div class="item-body">
                                        <ul class="details">
                                            <li><i class="fa fa-map-marker"></i><span>{{$publicaciones->ciudad->nombre}} - {{$publicaciones->ciudad->departamento->nombre}}</span></li>
                                            <li><i class="fa fa-money"></i><span>{{$publicaciones->salminimo}} - {{$publicaciones->salmaximo}} Euros</span></li>
                                            @if ($publicaciones->lenguaje_id == '')
                                            @else
                                            <li><i class="fa fa-globe"></i><span>{{$publicaciones->lenguaje->nombre}}</span></li>
                                            @endif
                                        </ul>
                                    </div>
                                </a>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </section>
@stop
