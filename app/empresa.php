<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Laravel\Scout\Searchable;
use App\vacantes;
use App\users;
use App\aplicacion;
use App\aplicantes;

class empresa extends Model
{
    protected $table = 'empresa';
    protected $fillable = ['nit', 'direccion', 'telefono', 'contacto','ciudad_id','descripcion','users_id','web','twitter','facebook','logo', 'estado'];

    use Searchable;

    public function users()
    {
        return $this->belongsTo(users::class);
    }

    public function aplicante()
    {
        return $this->hasOne(aplicantes::class);
    }

    public function vacantes()
    {
        return $this->hasMany(vacantes::class);
    }

    public function ciudad()
    {
        return $this->belongsTo(ciudad::class);
    }

    public function searchables()
    {
        return 'users-index';
    }

    public function toSearchableArray()
    {
        $array = $this->toArray();

        // Customize array...

        return $array;
    }
  
}
