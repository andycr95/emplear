<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReferenciasTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'referencias';

    /**
     * Run the migrations.
     * @table referencias
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('nombre', 45)->nullable()->default(null);
            $table->string('apellidos', 45)->nullable()->default(null);
            $table->string('ocupacion', 45)->nullable()->default(null);
            $table->integer('telefono')->nullable()->default(null);
            $table->string('estado', 45)->nullable()->default(null);
            $table->unsignedInteger('users_id');
            $table->unsignedInteger('relacion_id');

            $table->index(["users_id"], 'referencias_users_id_foreign');

            $table->index(["relacion_id"], 'referencias_relacion_id_foreign');


            $table->foreign('relacion_id', 'referencias_relacion_id_foreign')
                ->references('id')->on('relacion')
                ->onDelete('restrict')
                ->onUpdate('restrict');

            $table->foreign('users_id', 'referencias_users_id_foreign')
                ->references('id')->on('users')
                ->onDelete('restrict')
                ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
