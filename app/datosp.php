<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Laravel\Scout\Searchable;
use App\areas;
use App\licencia;
use App\users;

class datosp extends Model
{
    protected $table = 'datosp';
    protected $fillable = ['documento','telefono', 'fecha_nacimiento','direccion','users_id', 'tipodocumento_id','ciudad_id','estadocivil_id', 'genero_id','licencia_id', 'foto', 'estado'];

    use Searchable;
    
    public function tipodocumento(){
        return $this->belongsTo(tipodocumento::class);
    }
    public function ciudad(){
        return $this->belongsTo(ciudad::class);
    }
    public function estadocivil(){
        return $this->belongsTo(estadocivil::class);
    }
    public function users(){
        return $this->belongsTo(users::class);
    }
    public function genero(){
        return $this->belongsTo(genero::class);
    }
    public function licencia(){
        return $this->belongsTo(licencia::class);
    }
    public function areas(){
        return $this->belongsTo(areas::class);
    }

    public function Scopeciudades($query, $ciudades)
    {
        $ciudades = config('options.ciudades');

        if($ciudades)
            return $query->where('ciudad_id', 'LIKE', "%$ciudades%");  
    }

    public function searchables()
    {
        return 'users-index';
    }

    public function toSearchableArray()
    {
        $array = $this->toArray();

        // Customize array...

        return $array;
    }
  
}
