<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\users;
use App\role;

class assignedRole extends Model
{
    protected $table = 'assigned_roles';
    protected $fillable = ['users_id',  'role_id'];


    public function users(){
        return $this->belongsTo(users::class);
    }

    public function role()
    {
        return $this->belongsToMany(role::class);
    } 
}
