<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\datosp;
use App\users;
use App\empresa;
use App\areas;
use App\ciudad;
use App\vacantes;
use App\categoria;
use App\tipoempleo;
use App\lenguaje;
use App\role;
use App\aplicacion;


class vacantesController extends Controller
{

    public function show($id)
    {
        
        $vacantes = vacantes::find($id); 
        return view('jobs/job',  compact('vacantes'));
    }

    
    public function data($id)
    {
        
        $users = vacantes::find($id);  
        return view('principal', compact('users'));
    }

    public function index2($id)
    {

        $datos = empresa::find($id);
        $ciudad = ciudad::all();
        $categoria = categoria::all();
        $tipoempleo = tipoempleo::all();
        $vacantes = vacantes::find($id);
        $lenguaje = lenguaje::all();
        return view('empresa/vacantecreate', compact('datos','vacantes','lenguaje','categoria','tipoempleo', 'ciudad'));

    }

    public function index(request $request) 
    {   
        $buscar = $request->buscar;
        $nombre = $request->nombre;

        $ciudad = ciudad::get();
           
        if($buscar == '' && $nombre == ''){

            $vacantes = vacantes::paginate(7);
            $datosp = datosp::paginate(4);
            $datose = empresa::paginate(4);
        }
        
        
        else{

        
                
        if($buscar != '' && $nombre == ''){
            
            $vacantes = vacantes::where('ciudad_id' , $buscar)->paginate(7);
            $datosp = datosp::paginate(4);
            $datose = empresa::paginate(4);
  
        }       

        elseif($nombre != '' && $buscar == ''){
            
            $vacantes = vacantes::search($nombre)->paginate(7);
            $datosp = datosp::paginate(4);
            $datose = empresa::paginate(4);
  
        } 

        elseif($buscar != '' && $nombre != '') {
                
            $vacantes = vacantes::where('palabrasclave' , $nombre)->where('ciudad_id' , $buscar)->searchable()->paginate(7);
            $datosp = datosp::paginate(4);
            $datose = empresa::paginate(4);
        }

     }    
                
        
    
        return view('contenido/contenido', compact('vacantes', 'datose','datosp','ciudad'));
    
                
    }
    
    public function index3(request $request)
    {
        $buscar = $request->ciudad;
        $nombre = $request->carrera;

        $ciudad = ciudad::get();
           
        if($buscar == '' && $nombre == ''){

            $vacantes = vacantes::latest('fecha')->get();
            $ciudad = ciudad::all();
            $carrera = areas::all();
        }
        
        
        else{

        
                
        if($buscar != '' && $nombre == ''){
            
            $vacantes = vacantes::where('ciudad_id' , $buscar)->paginate(7);
            $ciudad = ciudad::all();
            $carrera = areas::all();
  
        }       

        elseif($nombre != '' && $buscar == ''){
            
            $vacantes = vacantes::search($nombre)->paginate(7);
            $ciudad = ciudad::all();
            $carrera = areas::all();
  
        } 

        elseif($buscar != '' && $nombre != '') {
                
            $vacantes = vacantes::search($nombre)->where('ciudad_id' , $buscar)->searchable()->paginate(7);
            $ciudad = ciudad::all();
            $carrera = areas::all();
        }

     }    
      
        return view('jobs/trabajos', compact('vacantes', 'ciudad', 'carrera'));
    }

    public function store(Request $request)
    {

        $vacantes = new vacantes();
        $vacantes->nombre = $request->nombre;
        $vacantes->palabrasclave = $request->palabrasclave;
        $vacantes->salmaximo = $request->salmaximo;
        $vacantes->salminimo = $request->salminimo;
        $vacantes->descripcion = $request->descripcion;
        $vacantes->rangoedad = $request->rangoedad;
        $vacantes->empresa_id = $request->empresa_id;
        $vacantes->vence = $request->vence;
        $vacantes->fecha = $request->fecha;
        $vacantes->experiencia = $request->experiencia;
        $vacantes->convocante = $request->convocante;
        $vacantes->tipoempleo_id = $request->tipoempleo_id;
        $vacantes->categoria_id = $request->categoria_id;
        $vacantes->cantidad = $request->cantidad;
        $vacantes->ciudad_id = $request->ciudad_id;
        $vacantes->lenguaje_id = $request->lenguaje_id;
        $vacantes->estado = 'activo';
        $vacantes->save();

        return back()->with('info', 'Su vacante ha sido publicada');

    }


}