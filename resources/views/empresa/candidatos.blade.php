@extends('empresa.principal')


@section('header')

<header class="site-header size-lg text-center" style="background-image: url(/img/candidatos.jpg)">
<div class="container page-name">
    <h1 class="text-center">Candidatos</h1>
    <p class="lead text-center">Estas son los candidatos que tenemos actualmente</p>
</div>
</header>
@stop
@section('content')
    <section class="no-padding-top bg-alt">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <br>
                </div>
                </div>
                <div class="row">
                <div class="col-md-4">
                    <div class="filter">
                        <form method="GET" action="/candidatos" accept-charset="UTF-8" class="header-job-search">
                        <div>
                            <h2>Ciudad</h2>
                            @foreach ($ciudad as $ciudades)
                            <div class="form">
                                <label for="nombre" class="control-label">{{ $ciudades->nombre }}</label>
                                <input type="checkbox" class="form" name="ciudad" value="{{ $ciudades->id }}">    
                            </div>
                            @endforeach
                        </div>
                        <div>
                            <h2>Carrera</h2>
                                @foreach ($carrera as $carrera)
                                <div class="form">
                                    <label for="nombre" class="control-label">{{ $carrera->nombre }}</label>
                                    <input type="checkbox" class="form" name="area" value="{{ $carrera->id }}">    
                                </div>
                                @endforeach
                            </div>
                        <div>
                            <h2>Experiencia</h2>
                                <div class="form">
                                    <label for="nombre" class="control-label">1 Año</label>
                                    <input type="checkbox" class="form" name="experiencia" value="1 año">    
                                </div>
                                <div class="form">
                                    <label for="nombre" class="control-label">2 Años</label>
                                    <input type="checkbox" class="form" name="experiencia" value="2 años">    
                                </div>
                                <div class="form">
                                    <label for="nombre" class="control-label">3 Años</label>
                                    <input type="checkbox" class="form" name="experiencia" value="3 años">    
                                </div>
                            </div>
                            <br>
                            <div>
                                <button class="btn btn-primary btn-xs" type="submit">Filtrar</button>
                                <button class="btn btn-danger btn-xs" type="summit">Todas</button>
                            </div>
                        </form>   
                        </div>
                    </div>
                <div class="col-xs-8">
                <div class="row grid-jobs">
                    @foreach($candidatos as $candidatos)
                    <div class="col-md-4 grid-item job">
                    <a class="item-block" href="/candidato/perfil/{{$candidatos->users_id}}">
                            <header>
                                    @if ( $candidatos->foto != '' )
                                    <img src="{{$candidatos->foto}}" alt="">    
                                    @else
                                    <img src="/img/avatar.jpg" alt="">  
                                    @endif 
                                <div class="hgroup">
                                    <h4>{{ $candidatos->users->name }} {{$candidatos->users->apellidos}}</h4>
                                </div>
                            </header>
                            <div class="item-body">
                                <ul class="details">
                                    <li><i class="fa fa-map-marker"></i><span>{{$candidatos->ciudad->nombre}}</span></li>
                                    <li><i class="fa fa-book"></i> {{$candidatos->areas->nombre}}<span></span></li>
                                </ul>
                            </div>
                        </a>
                    </div> 
                    @endforeach   
                </div>
            </div>
        </div>
    </section>
@stop