@extends('empresa.principal')

@section('header')
<header class="site-header size-lg text-center" style="background-image: url(/img/biuld.jpg)">
</header>
@stop


@section('content')
    <section>
        <div class="container">
                <div class="row">
                    <div class="col-md-10">                  
                        <h2>Acerca de {{$datos->users->name}}</h2>
                        <div class="form-group">
                            <label for="telefono" class="col-md-4 control-label">Razon social</label>
                            {{$datos->users->apellidos}}                        
                            </div>
                        <div class="form-group">
                                    <label for="telefono" class="col-md-4 control-label">Nit</label>
                                    {{$datos->nit}}                        
                                    </div>
                        <div class="form-group">
                            <label for="direccion" class="col-md-4 control-label">Direccion</label>
                            {{$datos->direccion}} 
                            </div>
                        <div class="form-group">
                                <label for="contacto" class="col-md-4 control-label">Contacto</label>
                                {{$datos->contacto}} 
                            </div>
                        <div class="form-group">
                            <label for="web" class="col-md-4 control-label">Pagina Web</label>
                            {{$datos->web}}
                            </div>
                        <div class="form-group">
                            <label for="facebook" class="col-md-4 control-label">Facebook</label>
                            {{$datos->facebook}} 
                            </div>
                        <div class="form-group">
                            <label for="twitter" class="col-md-4 control-label">twitter</label>
                            {{$datos->twitter}}  
                            </div>             
                        
                                  
                        
                    </div>
                    <div class="col-md-2">
                        <img src="{{$datos->logo}}" alt="">
                    </div>
                </div>    
                <hr />
                
                <div class="row">
                    <div class="col-md-12">
                        <div id="field_descripcion" class="form-group">
                            <h2 for="descripcion" class="control-label">Descripcion</h2>    
                            <div class="controls">
                                {{$datos->descripcion}} 
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </section>
@stop