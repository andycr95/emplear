<?php

namespace App\Http\Controllers;
use App\empresa;
use App\ciudad;
use App\vacantes;
use App\categoria;
use App\tipoempleo;
use App\lenguaje;



use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class empresaController extends Controller
{

    public function index2($id)
    {

        $datos = empresa::find($id);
        $ciudad = ciudad::all();
        $empresa = empresa::all();

        return view('empresa/datosempre', compact('datos','ciudad', 'empresa'));

    }

    public function perfil($id)
    {

        $datos = empresa::find($id);
        return view('empresa/perfil', compact('datos'));

    }

    public function perfil2($id)
    {

        $datos = empresa::find($id);
        return view('empleados/empresa-perfil', compact('datos'));

    }

    public function store(Request $request)
    {
        if (!$request->ajax()) return redirect('/');
        $empresa = new empresa();
        $empresa->nit = $request->nit;
        $empresa->telefono = $request->telefono;
        $empresa->direccion = $request->direccion;
        $empresa->contacto = $request->contacto;
        $empresa->descripcion = $request->descripcion;
        $empresa->web = $request->web;
        $empresa->twitter = $request->twitter;
        $empresa->users_id = $request->users_id;
        $empresa->facebook = $request->facebook;
        $empresa->ciudad_id = $request->ciudad_id;
        $empresa->lenguaje_id = $request->lenguaje_id;
        $empresa->estado = 'activo';
        $empresa->save();
        return $request->all();

    }

    public function index(Request $request)
    {
        $buscar = $request->buscar;
        $nombre = $request->nombre;

        $ciudad = ciudad::get();
           
        if($buscar == '' && $nombre == ''){

            $empresa = empresa::orderBy('created_at', 'desc')->paginate(10);
        }
        else{  
                
        if($buscar != '' && $nombre == ''){
            
            $empresa = empresa::where('ciudad_id' , $buscar)->paginate(10);
  
        }       

        elseif($nombre != '' && $buscar == ''){
            
             $empresa = empresa::search($nombre)->paginate(10);
  
  
        } 

        elseif($buscar != '' && $nombre != '') {
                
            $empresa = empresa::search($nombre)->where('ciudad_id' , $buscar)->paginate(10);
  
  
        }

     }    
        
   

     return view('empleados/empresas', compact('empresa', 'ciudad'));


    }
    
}
