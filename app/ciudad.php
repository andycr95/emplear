<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\departamento;

class ciudad extends Model
{
    protected $table = 'ciudad';
    protected $fillable = ['codigo','nombre', 'departamento_id', 'estado'];


    public function departamento()
    {
        return $this->belongsTo(departamento::class);
    }

    public function empresa()
    {
        return $this->hasMany(empresa::class);
    }

    public function vacantes()
    {
        return $this->hasMany(vacantes::class);
    }

    public function datosp()
    {
        return $this->hasMany(datosp::class);
    }

    public function estudios()
    {
        return $this->hasMany(estudios::class);
    }

    public function cursos()
    {
        return $this->hasMany(cursos::class);
    }
}
