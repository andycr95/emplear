<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class anio extends Model
{
    protected $table = 'anio';
    protected $fillable = ['nombre', 'estado'];

    public function estudios()
    {
        return $this->hasMany(estudios::class);
    }

    public function cursos()
    {
        return $this->belongsTo(cursos::class);
    }

    public function ciudad()
    {
        return $this->belongsTo(ciudad::class);
    }

    public function users()
    {
        return $this->belongsTo(usersss::class);
    }

}
