<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIdiomasTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'idiomas';

    /**
     * Run the migrations.
     * @table idiomas
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('nombre', 45)->nullable()->default(null);
            $table->string('habla', 45)->nullable()->default(null);
            $table->string('estado', 45)->nullable()->default(null);
            $table->string('lee', 45)->nullable()->default(null);
            $table->string('escribe', 45)->nullable()->default(null);
            $table->unsignedInteger('users_id');
            $table->unsignedInteger('lenguaje_id');

            $table->index(["users_id"], 'idiomas_users_id_foreign');

            $table->index(["lenguaje_id"], 'idiomas_lenguaje_id_foreign');


            $table->foreign('lenguaje_id', 'idiomas_lenguaje_id_foreign')
                ->references('id')->on('lenguaje')
                ->onDelete('restrict')
                ->onUpdate('restrict');

            $table->foreign('users_id', 'idiomas_users_id_foreign')
                ->references('id')->on('users')
                ->onDelete('restrict')
                ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
