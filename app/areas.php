<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\users;
use App\areas;

class areas extends Model
{
    protected $table = 'areas';
    protected $fillable = ['nombre', 'estado', 'areascol'];

    public function users()
    {
        return $this->hasMany(users::class);
    }

    public function areas()
    {
        return $this->hasMany(areas::class);
    }
}
