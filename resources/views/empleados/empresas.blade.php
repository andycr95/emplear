@extends('empleados.principal')


@section('header')
<header class="site-header size-lg text-center" style="background-image: url(/img/empresa.png)">
<div class="container page-name">
    <h1 class="text-center">Empresas</h1>
    <p class="lead text-center">Estas son los empresas que tenemos actualmente</p>
    <br><br><br>
    <form method="GET" action="/empresas" accept-charset="UTF-8" class="header-job-search">
        <div class="input-keyword">
            <input class="form-control" name="nombre" value="">
        </div> 
        <div class="input-location">
            <select class="form-control" name="buscar" id="">
                <option name="buscar" value="">Seleccione una ciudad</option>
                @foreach ($ciudad as $ciudad)
                <option name="buscar" value="{{ $ciudad->id }}">{{ $ciudad->nombre }}</option>  
                @endforeach
            </select>
        </div> 
        <div class="btn-search">
            <button class="btn btn-primary" type="submit">Buscar</button>
        </div>
    </form>
</div>
</header>
@endsection
@section('content')
    <section class="no-padding-top bg-alt">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <br>
                </div>
            <div class="col-xs-12">
                <div class="row grid-jobs">
                    @foreach($empresa as $empresa)
                    <div class="col-md-4 grid-item job">
                    <a class="item-block" href="/empresa/perfil/{{$empresa->users_id}}">
                            <header>
                                <img src="{{$empresa->logo}}" alt="">
                                <div class="hgroup">
                                    <h4>{{ $empresa->users->name }} {{$empresa->users->apellidos}}</h4>
                                    <h5>{{$empresa->users->email}}</h5>
                                    @if ($empresa->vacantes->count() != 0 )
                                    <span class="label label-success">{{ $empresa->vacantes->count() }} Ofertas</span>   
                                        
                                    @else
                                    <span class="label label-danger">{{ $empresa->vacantes->count() }} Ofertas</span>
                                    @endif
                                </div>
                            </header>
                            <div class="item-body">
                                <ul class="details">
                                    <li><i class="fa fa-map-marker"></i><span>{{$empresa->ciudad->nombre}}</span></li>
                                    <li><i class="fa fa-internet-explorer"></i>{{$empresa->web}}<span></span></li>
                                    <li><i class="fa fa-phone"></i>{{$empresa->telefono}}<span></span></li>
                                </ul>
                            </div>
                        </a>
                    </div> 
                    @endforeach   
                </div>
            </div>
        </div>
    </section>
@stop