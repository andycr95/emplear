@extends('admin.admin')
@section('content')
<h1>Editar</h1>
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Usuarios</h3>
              
              <div class="box-tools">
                <form action="/admin">
                  @csrf
                  <input type="hidden" id="remember_token" name="remember_token" value="">
                  <div class="input-group input-group-sm" style="width: 150px;">
                    
                    <div class="input-keyword">
                      <input type="text" name="search" class="form-control pull-right" placeholder="Search">
                    </div>
                    <div class="input-group-btn">
                      
                      <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                    </div>
                    
                  </div>
                </form>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
              <table class="table table-hover">
                <tbody>
                  <tr>
                    <th>ID</th>
                    <th>Nombre</th>
                    <th>Email</th>
                    <th>Estado</th>
                    <th>Role</th>
                  </tr>
                  <tr>  
                    <td>{{ $datos->id }}</td>
                    <td>{{ $datos->name }}</td>
                    <td>{{ $datos->email }}</td>
                    @if ($datos->estado == 'activo')
                    <td><span class="label label-success">{{ $datos->estado }}</span></td>
                    @else
                    <td><span class="label label-danger">{{ $datos->estado }}</span></td>
                    @endif          
                    <td>{{ $datos->roles->pluck('displayname')->implode(',') }}</td>                  
                  </tr>
                </tbody>
              </table>  
            </div>
          </div>
          <div class="row">
            <div class="col-md-6">
              <div class="box">
                <div class="box-header with-border">
                  <h3 class="box-title">Acciones</h3>
                </div>
                  <!-- /.box-header -->
                  <div class="box-body">
                    <table class="table table-bordered">
                      <tbody>
                          @if (session()->has('info'))
                            <div class="alert alert-success">
                            {{ session('info') }}
                            </div>
                          @endif
                        <tr>
                        @if ($datos->assignedRole != '')
                        @else
                        <th>Agregar Role</th>
                        @endif
                          <th>Cambiar Estado</th>
                        </tr>
                        <tr>
                        @if ($datos->assignedRole != '')
                        @else
                        <td>
                          {!! Form::open([ 'method' => 'POST', 'action' =>['adminController@rolestore', $datos->id] ]) !!}
                            {!! Form::hidden('users_id', $datos->id) !!}
                            {!! Form::select('role_id' , array("1" => "Candidato", "3" => "Empresa", "2" => "Administrador"), "Candidato") !!}
                            {!! Form::submit('Guardar') !!}
                        {!! Form::close() !!}
                        </td>
                        @endif
                          <td>
                            {!! Form::open([ 'method' => 'POST', 'action' =>['adminController@editestado', $datos->id] ]) !!}
                              @foreach ($estado as $estado)
                              @if ($estado == 'activo')
                              
                              {!! Form::checkbox("estado", $estado) !!}<span class="label label-success">{{ $estado }}</span>
                              @elseif ($estado == 'inactivo')
                              {!! Form::checkbox("estado", $estado) !!}<span class="label label-danger">{{ $estado }}</span>
                              @endif
                              @endforeach
                              {!! Form::submit('Guardar', ['class' => 'btn btn-primary btn-xs']) !!}
                            {!! Form::close() !!}
                          </td>
                        </tr>
                      </tbody>
                    </table>     
                </div>
              </div>
            </div>
          </div>
@endsection