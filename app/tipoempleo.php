<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class tipoempleo extends Model
{
    protected $table = 'tipoempleo';
    protected $fillable = ['nombre', 'estado'];

    public function vacantes()
    {
        return $this->hasMany(vacantes::class);
    }
}
