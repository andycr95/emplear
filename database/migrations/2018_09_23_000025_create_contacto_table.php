<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContactoTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'contacto';

    /**
     * Run the migrations.
     * @table contacto
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('nombre', 45)->nullable()->default(null);
            $table->integer('telefono')->nullable()->default(null);
            $table->string('estado', 45)->nullable()->default(null);
            $table->string('direccion', 60)->nullable()->default(null);
            $table->unsignedInteger('datosperso_id');

            $table->index(["datosperso_id"], 'contacto_datosperso_id_foreign');


            $table->foreign('datosperso_id', 'contacto_datosperso_id_foreign')
                ->references('id')->on('datosp')
                ->onDelete('restrict')
                ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
