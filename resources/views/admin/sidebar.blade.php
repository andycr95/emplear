
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
            @if (auth::user()->datosp == '')
            <img src="/img/avatarpeque.jpg" class="img-circle" alt="User Image">
                
            @else
            <img src="{{ auth::user()->datosp->foto }}" class="img-circle" alt="User Image">
                
            @endif
        </div>
        <div class="pull-left info">
          <p>{{ auth::user()->name }} {{ auth::user()->apellidos }}</p>

          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <!-- search form -->
      <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
          <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
        </div>
      </form>
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>
        <li class="active treeview">
          <a href="#">
            <i class="fa fa-users"></i> <span>Usuarios</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="active"><a href="/admin/users/"><i class="fa fa-user"></i> Todos los usuarios</a></li>
          </ul>
        </li>
        <li class="active treeview">
            <a href="#">
              <i class="fa fa-industry"></i> <span>Empresas</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              <li class="active"><a href="/admin/empresas"><i class="fa fa-build"></i> Todas las Empresas</a></li>
            </ul>
        </li>
        <li class="active treeview">
            <a href="#">
              <i class="fa fa-book"></i> <span>Empleos</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              <li class="active"><a href="/admin/empleos/categorias"><i class="fa fa-book"></i> Categorias</a></li>
            </ul>
        </li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>