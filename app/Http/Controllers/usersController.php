<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\datosp;
use App\ciudad; 
use App\users;
use App\estudios;
use App\areas;
use App\empleo;

class usersController extends Controller
{
    public function index(Request $request)
    {
        $buscar = $request->ciudad;
        $nombre = $request->area;
        $experiencia = $request->experiencia;

        $ciudad = ciudad::all();
        $carrera = areas::all();
           
        if($buscar == '' && $nombre == '' && $experiencia == ''){

            $candidatos = datosp::orderBy('created_at', 'desc')->get();
            
        }
        else{  
                
        if($buscar != '' && $nombre == '' && $experiencia == ''){
            
            $candidatos = datosp::where('ciudad_id' , $buscar)->get();
  
        }       

        elseif($nombre != '' && $buscar == '' && $experiencia == ''){
            
             $candidatos = datosp::where('areas_id' , $nombre)->get();
  
  
        }
        
        elseif($nombre == '' && $buscar == '' && $experiencia != ''){
            
            $candidatos = datosp::where('años_labor' , $experiencia)->get();
 
 
        } 

        elseif($buscar != '' && $nombre != '' && $experiencia == '') {
                
            $candidatos = datosp::where('areas_id' , $nombre)->where('ciudad_id' , $buscar)->get();;
        }

        elseif($buscar == '' && $nombre != '' && $experiencia != '') {
                
            $candidatos = datosp::where('años_labor' , $experiencia)->where('areas_id' , $nombre)->get();;
        }

        elseif($buscar != '' && $nombre == '' && $experiencia != '') {
                
            $candidatos = datosp::where('ciudad_id' , $buscar)->where('años_labor' , $experiencia)->get();;
        }

        elseif($buscar != '' && $nombre != '' && $experiencia != '') {
                
            $candidatos = datosp::where('areas_id' , $nombre)->where('ciudad_id' , $buscar)->where('años_labor' , $experiencia)->get();;
  
  
        }

     }    
        
   

     return view('empresa/candidatos', compact('candidatos', 'ciudad', 'carrera'));


    }

    public function perfil($id)
    {        
        $datos = users::find($id);
        $dato = estudios::where('users_id', $id)->get();
        $emple = empleo::where('users_id', $id)->get();
        return view('empresa.cv-candidato', compact('datos', 'dato', 'emple'));
    }

    public function perfil2($id)
    {        
        $datos = users::find($id);
        $dato = estudios::where('users_id', $id)->get();
        $emple = empleo::where('users_id', $id)->get();
        return view('empleados.perfil', compact('datos', 'dato', 'emple'));
    }

    

}
