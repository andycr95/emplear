@extends('empresa.principal')

@if (auth::user()->estado == 'activo')
    @section('header')
        <header class="site-header size-lg text-center" style="background-image: url(/img/publi.jpg)">
            <div class="container page-name">
                <h1 class="text-center">Publica vacantes</h1>
            </div>
        </header>
    @endsection


    @section('content')
        <section>  
                @if (session()->has('info'))
                <div class="alert alert-success">
                    {{ session('info') }}
                </div>
                @endif
            <div class="container">
                <form class="form-horizontal" role="form" method="POST" action="{{ route('vacantecreate')}}">
                    @csrf
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group {{ $errors->has('nombre') ? ' is-invalid' : '' }}">
                                    <label for="nombre" class="col-md-4 control-label">Nombre</label>
                                        <div class="col-md-6">
                                            <input id="nombre" type="text" class="form-control" name="nombre" value="" required autofocus>
                                        </div>
                                    </div>
                                <div class="form-group">
                                    <label for="salminimo" class="col-md-4 control-label">Salario minimo</label>
                                        <div class="col-md-6">
                                            <input id="salminimo" type="text" class="form-control" name="salminimo" >
                                        </div>
                                </div>
                                <div class="form-group">
                                    <label for="salmaximo" class="col-md-4 control-label">Salario maximo</label>
                                        <div class="col-md-6">
                                            <input id="salmaximo" type="text" class="form-control" name="salmaximo" >
                                        </div>
                                    </div>
                                <div class="form-group">
                                    <label for="rangoedad" class="col-md-4 control-label">Rango de edad</label>
                                        <div class="col-md-6">
                                            <input id="rangoedad" type="text" class="form-control" name="rangoedad" value="" required autofocus>
                                        </div>
                                    </div>
                                <div class="form-group">
                                    <label for="cantidad" class="col-md-4 control-label">Cantidad de vacantes</label>
                                        <div class="col-md-6">
                                            <input id="cantidad" type="text" class="form-control" name="cantidad" value="" required autofocus>
                                        </div>
                                    </div>
                                <div class="form-group">
                                    <label for="categoria_id" class="col-md-4 control-label" name="categoria_id">Categoria</label>
                                        <div class="col-md-6">
                                            <select name="categoria_id" class="form-control">
                                                <option class="form-inputs" name="" value="#">Seleccione una opcion</option>
                                                @foreach($categoria as $categoria)
                                                <option  id="categoria_id" class="form-inputs" name="categoria_id" value="{{ $categoria->id }}" >{{$categoria->nombre}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                <div class="form-group">
                                    <label for="ciudad_id" class="col-md-4 control-label" name="ciudad_id">Ciudad</label>
                                        <div class="col-md-6">
                                            <select name="ciudad_id" class="form-control">
                                                <option class="form-inputs" name="" value="#">Seleccione una opcion</option>
                                                    @foreach($ciudad as $ciudad)
                                                        <option  id="ciudad_id" class="form-inputs" name="ciudad_id" value="{{ $ciudad->id }}" >{{$ciudad->nombre}}</option>
                                                    @endforeach
                                            </select>
                                        </div>
                                    </div>
                                <div class="form-group">
                                    <label for="tipoempleo_id" class="col-md-4 control-label" name="tipoempleo_id">Tipo de empleo</label>
                                        <div class="col-md-6">
                                            <select name="tipoempleo_id" class="form-control">
                                                <option class="form-inputs" name="" value="#">Seleccione una opcion</option>
                                                    @foreach($tipoempleo as $tipoempleo)
                                                        <option  id="tipoempleo_id" class="form-inputs" name="tipoempleo_id" value="{{ $tipoempleo->id }}" >{{$tipoempleo->nombre}}</option>
                                                    @endforeach
                                            </select>
                                        </div>
                                    </div>
                                <div class="form-group">
                                    <label for="lenguaje_id" class="col-md-4 control-label" name="lenguaje_id">Lenguaje</label>
                                        <div class="col-md-6">
                                            <select name="lenguaje_id" class="form-control">
                                                <option class="form-inputs" name="" value="#">Seleccione una opcion</option>
                                                    @foreach($lenguaje as $lenguaje)
                                                <option  id="lenguaje_id" class="form-inputs" name="lenguaje_id" value="{{ $lenguaje->id }}" >{{$lenguaje->nombre}}</option>
                                                    @endforeach
                                            </select>
                                        </div>
                                    </div>
                                <div class="form-group">
                                    <label for="experiencia" class="col-md-4 control-label">Tiempo de experiencia</label>
                                        <div class="col-md-6">
                                            <input id="experiencia" type="text" class="form-control" name="experiencia" value="" required autofocus>
                                        </div>
                                    </div>
                                <div class="form-group">
                                    <label for="fecha" class="col-md-4 control-label">Fecha de contratacion</label>
                                        <div class="col-md-6">
                                            <input id="fecha" type="date" class="form-control" name="fecha" value="" required autofocus>
                                        </div>
                                    </div>
                                <div class="form-group">
                                    <label for="convocante" class="col-md-4 control-label">Convocante</label>
                                        <div class="col-md-6">
                                            <input id="convocante" type="text" class="form-control" name="convocante" value="" required autofocus>
                                    </div>
                                    </div>
                                    
                                
                        </div>
                    </div>
                                    <hr />
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div id="field_descripcion" class="form-group">
                                                <label for="descripcion" class="control-label">Descripcion.</label>    
                                                    <div class="controls">
                                                        <textarea id="descripcion" class="form-control" name="descripcion" cols="50" rows="10"></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <div class="col-md-12 text-center">
                                        <button type="submit" id="empresa_id" name="empresa_id" class="btn btn-primary" value="{{auth::user()->id}}" >Guardar</button>
                                    </div>
                                </form>
                            </div>   
                        </div>
                </div>
                    </div>
                </div>
            </div> 
            <hr />    
        </section>
    @endsection
@elseif(auth::user()->estado == 'inactivo')
    @section('header')
        <header class="site-header size-lg text-center" style="background-image: url(/img/publi.jpg)">
            <div class="container page-name">
                <h1 class="text-center">Actualice su membresia</h1>
                <h3 class="text-center">Es necesaria para publicar vacantes</h3>
            </div>
        </header>
    @endsection
@endif