
@if(auth()->user()->hasRole('empresa'))
<section class="bg-img text-center" style="background-image: url(/img/banner1.jpg)">    
	<div class="container">
		<h2><strong>Últimos desarrolladores registrados</strong></h2>
		<h6 class="font-alt">Estos son algunos de los talentos registrados en nuestra plataforma</h6>
		<div class="container">
			<div class="row">   
				@foreach($datosp as $datosp)         
					<div class="col-md-3">
						<div class="thumbnail">
								@if ( $datosp->foto != '' )
                                <img src="{{$datosp->foto}}" alt="">    
                                @else
                                <img src="/img/avatar.jpg" alt="">  
                                @endif 
							<div class="">
								<h6>{{$datosp->users->name}}</h6>
								<a href="/candidato/perfil/{{$datosp->users->id}}" class="btn btn-primary">Ver Perfil</a>
								<br>
							</div>    
						</div>    
					</div>
				@endforeach
			</div>    
		</div>
	</div>
</section>
@else(auth()->user()->hasRole(['candidato', 'admin']))
<section class="bg-img text-center" style="background-image: url(/img/banner1.jpg)">    
	<div class="container">
		<h2><strong>Últimas empresas registradas</strong></h2>
		<h6 class="font-alt">Estos son algunos de los empleadores registrados en nuestra plataforma</h6>
		<div class="container">
			<div class="row">   
				@foreach($datose as $datose)         
					<div class="col-md-3">
						<div class="thumbnail">
							<img src="{{$datose->logo}}" alt="" class="profile-pic-list">   
							<div class="">
								<h6>{{$datose->users->name}}</h6>
								<a href="/empresa/perfil/{{$datose->id}}" class="btn btn-primary">Ver Perfil</a>
								<br>
							</div>    
						</div>    
					</div>
				@endforeach
			</div>   
		</div>
	</div>
</section>
@endif
