<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCursosTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'cursos';

    /**
     * Run the migrations.
     * @table cursos
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('nombre', 45)->nullable()->default(null);
            $table->string('institucion', 45)->nullable()->default(null);
            $table->string('horas', 45)->nullable()->default(null);
            $table->string('habilidades')->nullable()->default(null);
            $table->string('estado', 45)->nullable()->default(null);
            $table->unsignedInteger('users_id');
            $table->unsignedInteger('ciudad_id');
            $table->unsignedInteger('anio_id');

            $table->index(["anio_id"], 'cursos_anio_id_foreign');

            $table->index(["ciudad_id"], 'cursos_ciudad_id_foreign');

            $table->index(["users_id"], 'cursos_users_id_foreign');
            $table->nullableTimestamps();


            $table->foreign('anio_id', 'cursos_anio_id_foreign')
                ->references('id')->on('anio')
                ->onDelete('restrict')
                ->onUpdate('restrict');

            $table->foreign('ciudad_id', 'cursos_ciudad_id_foreign')
                ->references('id')->on('ciudad')
                ->onDelete('restrict')
                ->onUpdate('restrict');

            $table->foreign('users_id', 'cursos_users_id_foreign')
                ->references('id')->on('users')
                ->onDelete('restrict')
                ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
