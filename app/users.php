<?php

namespace App;

use Spatie\Permission\Traits\HasRoles;
use Illuminate\Notifications\Notifiable;
use Laravel\Scout\Searchable;
use Illuminate\Foundation\Auth\user as Authenticatable;
use App\empresa;
use App\aplicacion;
use App\estudios;
use App\empleo;
use App\assignedRole;
use App\aplicantes;

class users extends Authenticatable
{
    use Notifiable, HasRoles, searchable;

    protected $table = 'users';
    protected $fillable = ['name', 'apellidos', 'email','estado','password'];
    protected $hidden =['rememberToken'];
    
    
    public function assignedRole()
    {
        return $this->hasOne(assignedRole::class);
    }

    public function aplicaciones()
    {
        return $this->hasOne(aplicacion::class);
    }

    public function aplicante()
    {
        return $this->hasOne(aplicantes::class);
    }

    public function cursos()
    {
        return $this->hasmany(cursos::class);
    }

    
    public function empresa()
    {
        return $this->hasone(empresa::class);
    }

    public function roles()
    {
        return $this->belongsToMany(role::class, 'assigned_roles');
    } 

    
    public function searchables()
    {
        return 'users-index';
    }

    public function toSearchableArray()
    {
        $array = $this->toArray();

        // Customize array...

        return $array;
    }
    

    public function datosp()
    {
        return $this->hasOne(datosp::class);
    }

    public function estudios()
    {
        return $this->hasMany(estudios::class);

    }

    public function empleos()
    {
        return $this->hasMany(empleo::class);

    }

}
