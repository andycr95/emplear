<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\users;
use App\assignedRole;



class role extends Model
{
    protected $fillable = [
        'name', 'diplayname'
    ];

    public function assignedRole(){

            return $this->hasMany(assignedRole::class);
        }
        
    
}
