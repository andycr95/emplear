@extends('empleados.principal')      
    @if(Auth::user()->datosp != '')
        @section('header')
        <header class="site-header size-lg text-center" style="background-image: url(/img/curriculum.jpg)">
            <div class="container page-name">
                <h1 class="text-center">Hola {{ auth::user()->name}}</h1>
            </div>
            @if ( auth::user()->datosp->foto != '' )
                <img src="{{ auth::user()->datosp->foto}}" alt="">    
            @else
                <img src="/img/avatar.jpg" alt="">  
            @endif
        </header>
        @stop
        @section('content')
            <section>
                <div class="container">
                    <div class="container">
                        <h2  class="control-label">Datos Personales</h2>                  
                            <div class="form-group">
                                <label for="telefono" class="col-md-4 control-label">Nombre completo</label>
                                {{ auth::user()->name }} {{ auth::user()->name }}
                                </div>
                            <div class="form-group">
                                <label for="telefono" class="col-md-4 control-label">Numero telefonico</label>
                                {{ auth::user()->datosp->telefono }}
                                </div>
                            <div class="form-group">
                                <label for="web" class="col-md-4 control-label">{{ auth::user()->datosp->tipodocumento->nombre }}</label>
                                {{ auth::user()->datosp->documento }}
                                </div>
                            <div class="form-group">
                                <label for="contacto" class="col-md-4 control-label">Profesion</label>
                                {{ auth::user()->datosp->areas->nombre }}
                                </div>
                            <div class="form-group">
                                <label for="contacto" class="col-md-4 control-label">Licencia</label>
                                {{ auth::user()->datosp->licencia->nombre }} - {{ auth::user()->datosp->numero_licencia }} 
                                </div>     
                            <div class="form-group">
                                <label for="facebook" class="col-md-4 control-label">Estado civil</label>
                                {{ auth::user()->datosp->estadocivil->nombre }}
                                </div>
                            <div class="form-group">
                                <label for="twitter" class="col-md-4 control-label">Ciudad</label>
                                {{ auth::user()->datosp->ciudad->nombre }} - {{ auth::user()->datosp->ciudad->departamento->nombre }}
                                </div>            
                            <div class="form-group">
                                <label for="nit" class="col-md-4 control-label">Genero</label>
                                {{ auth::user()->datosp->genero->nombre }}
                                </div>   
                    
                        
                            <div id="field_descripcion" class="form-group">
                                <h2 for="descripcion" class="control-label">Descripcion</h2>  
                                <div class="controls">
                                    {{ auth::user()->datosp->descripcion }}
                                </div>
                            </div>
                            <br><br>
                    </div>
                        <div class="row">
                                <div class="col-md-6">
                                    <h2 for="descripcion" class="control-label">Formacion Academica</h2> 
                                    @foreach ($dato as $dato)
                                        
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div id="field_descripcion" class="form-group">
                                                @if($dato->nivel_id == 1)
                                                <h2 for="descripcion" class="control-label">Primaria</h2>
                                                @elseif($dato->nivel_id == 2)
                                                <h2 for="descripcion" class="control-label">Secundaria</h2>  
                                            @endif      
                                            </div>
                                            <div class="form-group">
                                                @if($dato->institucion != '')
                                                <label for="intitucion" class="col-md-4 control-label">Nombre Intitucion</label>
                                                {{$dato->institucion}} 
                                            </div>
                                            @else
                                            @endif   
                                            <div class="form-group">
                                                @if($dato->enfasis != '')
                                                <label for="intitucion" class="col-md-4 control-label">enfasis</label>
                                                {{$dato->enfasis}} 
                                            </div>
                                            @else
                                            @endif 
                                            <div class="form-group">
                                                <label for="intitucion" class="col-md-4 control-label">Ciudad</label>  
                                                {{$dato->ciudad->nombre}} 
                                            </div>
                                            <div class="form-group">
                                                <label for="intitucion" class="col-md-4 control-label">Estado</label>  
                                                {{$dato->estado->nombre}} 
                                            </div> 
                                            <div class="form-group">
                                                <label for="intitucion" class="col-md-4 control-label">Año</label>  
                                                {{$dato->anio->nombre}} 
                                            </div>      
                                        </div>   
                                    </div>
                                    @endforeach
                                </div>
                                <div class="col-md-6">
                                    @if (auth::user()->empleos != '')
                                        
                                    <h2 for="descripcion" class="control-label">Experiencia laboral</h2> 
                                    <div class="panel-group" id="accordion">
                                        @foreach ($emple as $emple)
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h4 class="panel-title">
                                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse{{ $emple->id }}">{{ $emple->titulo }}</a>
                                                </h4>
                                            </div>
                                            <div id="collapse{{ $emple->id }}" class="panel-collapse collapse">
                                                <div class="panel-body">
                                                    <div class="container"> 
                                                        <div class="form-group">
                                                            @if($emple->nombre != '')
                                                                <label for="nombre" class="col-md-4 control-label">Nombre</label>
                                                                    {{$emple->nombre}} 
                                                            </div>
                                                            @else
                                                            @endif 
                                                        <div class="form-group">
                                                            @if($emple->empresa != '')
                                                                <label for="empresa" class="col-md-4 control-label">Empresa</label>
                                                                    {{$emple->empresa}} 
                                                            </div>
                                                            @else
                                                            @endif 
                                                        <div class="form-group">
                                                            @if($emple->telefono != '')
                                                                <label for="telefono" class="col-md-4 control-label">Telefono</label>
                                                                {{$emple->telefono}} 
                                                            </div>
                                                            @else
                                                            @endif 
                                                        <div class="form-group">
                                                            @if($emple->fecha_inicio != '')
                                                                <label for="fecha_inicio" class="col-md-4 control-label">Fecha inicio</label>
                                                                {{$emple->fecha_inicio}} 
                                                            </div>
                                                            @else
                                                            @endif 
                                                        <div class="form-group">
                                                            @if($emple->fecha_fin != '')
                                                                <label for="fecha_fin" class="col-md-4 control-label">Fecha fin</label>
                                                                {{$emple->fecha_fin}} 
                                                            </div>
                                                            @else
                                                            @endif 
                                                        <div class="form-group">
                                                                <label for="categoria" class="col-md-4 control-label">categoria</label>  
                                                                {{$emple->categoria->nombre}} 
                                                            </div>
                                                        <div class="form-group">
                                                                <label for="intitucion" class="col-md-4 control-label">Ciudad</label>  
                                                                {{$emple->ciudad->nombre}} 
                                                            </div>
                                                        <div class="form-group">
                                                            <label for="intitucion" class="col-md-4 control-label">Tipo de contrato</label>  
                                                            {{$emple->tipoempleo->nombre}} 
                                                            </div>      
                                                    </div>  
                                                </div>
                                            </div> 
                                        </div>
                                        @endforeach
                                    </div>
                                    @else
                                        
                                    @endif
                                </div>
                            </div>
                        </div>
                    </section>
                    @stop
                    @else
                    @section('header')
                    <header class="site-header size-lg text-center" style="background-image: url(/img/curriculum.jpg)">
                        <div class="container page-name text-center">
                            <h1 class="text-center">Hola</h1>
                            <img src="/img/warning.png">
                <h2 class="text-center">Debes llenar tu datos personales</h2>
            </div>
            <div class="container">
                <p class="text-center"><a class="btn btn-info" href="/hojav/{{auth::user()->id}}">Datos Personales</a></p>
            </div>
        </header>
        @stop
    @endif
