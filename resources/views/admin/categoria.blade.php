@extends('admin.admin')
@section('content')
  <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div class="box-header">
            <div class="card-header">
                <i class="fa fa-align-justify"></i> Categorías
                <button type="button" class="btn btn-secondary" data-toggle="modal" data-target="#modalNuevo">
                    <i class="icon-plus"></i>&nbsp;Nuevo
                </button>
            </div>
            <div class="box-tools">
              <form action="/admin">
                <div class="input-group input-group-sm" style="width: 150px;">
                  
                  <div class="input-keyword">
                    <input type="text" name="search" class="form-control pull-right" placeholder="Search">
                  </div>
                  <div class="input-group-btn">
                    
                    <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                  </div>
                  
                </div>
              </form>
            </div>
          </div>
          <!-- /.box-header -->
          <div class="box-body table-responsive no-padding">
            <table class="table table-hover">
              <tbody><tr>
                <th>ID</th>
                <th>Nombre</th>
                <th>Estado</th>
              </tr>
              <tr>
                
                @foreach ($categorias as $categoria)
                
                <tr>  
                  <td>{{ $categoria->id }}</td>
                  <td><a href="/admin/usershow/{{$categoria->id}}">{{ $categoria->nombre }}</a></td>
                  @if ($categoria->estado == 'activo')
                  <td><span class="label label-success">{{ $categoria->estado }}</span></td>
                  @else
                  <td><span class="label label-danger">{{ $categoria->estado }}</span></td>
                  @endif                  
                </tr>
                @endforeach
              </tbody>
            </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
      </div>  
    <!-- right col -->
  </div>
  <div class="modal fade" id="modalNuevo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="display: none;" aria-hidden="true">
    <div class="modal-dialog modal-primary modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Agregar categoría</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">×</span>
                </button>
            </div>
            <form action="{{ route('admin.categoria.create') }}" method="post" enctype="multipart/form-data" class="form-horizontal">
                @csrf
                <div class="modal-body">
                    <div class="form-group row">
                        <label class="col-md-3 form-control-label" for="text-input">Nombre</label>
                        <div class="col-md-9">
                            <input type="text" id="nombre" name="nombre" class="form-control" placeholder="Nombre de categoría">
                            <span class="help-block">(*) Ingrese el nombre de la categoría</span>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                    <button class="btn btn-primary" type="submit">Guardar</button>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
  <!-- /.row (main row) -->
@endsection