@extends('admin.admin')
@section('content')
  <div class="row">
    <div class="col-xs-12">
        <div class="box">
          <div class="box-header">
            <h3 class="box-title">Empresas</h3>
            
            <div class="box-tools">
              <form action="/admin">
                <div class="input-group input-group-sm" style="width: 150px;">
                  
                  <div class="input-keyword">
                    <input type="text" name="search" class="form-control pull-right" placeholder="Search">
                  </div>
                  <div class="input-group-btn">
                    
                    <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                  </div>
                  
                </div>
              </form>
            </div>
          </div>
          <!-- /.box-header -->
          <div class="box-body table-responsive no-padding">
            <table class="table table-hover">
              <tbody><tr>
                <th>ID</th>
                <th>Nombre</th>
                <th>Estado</th>
                <th>Numero de ofertas</th>
              </tr>
              <tr>
                
                @foreach ($empresa as $empresa)
                
                <tr>  
                  <td>{{ $empresa->id }}</td>
                  <td><a href="/admin/empresashow/{{$empresa->users->id}}">{{ $empresa->users->name }}</a></td>
                  @if ($empresa->estado == 'Activo')
                  <td><span class="label label-success">{{ $empresa->estado }}</span></td>
                  @elseif($empresa->estado == 'Inactivo')
                  <td><span class="label label-danger">{{ $empresa->estado }}</span></td>
                  @endif                   
                  <td>{{ $empresa->vacantes->count() }}</td>
                </tr>
                @endforeach
              </tbody>
            </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
      </div>  
    <!-- right col -->
  </div>
  <!-- /.row (main row) -->
@endsection