@extends('jobs.principal')
@section('header')
<header class="site-header size-lg text-center" style="background-image: url(/img/ofertas.jpg)">
   
</header> 
@endsection
@section('content')
    <section class="no-padding-top bg-alt">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <br>
                    <h5>Encontramos <strong>{{ $vacantes->count() }}</strong> ofertas</h5>
                    </div>
                </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="filter">
                    <form method="GET" action="/jobs" accept-charset="UTF-8" class="header-job-search">
                    <div>
                        <h2>Ciudad</h2>
                        @foreach ($ciudad as $ciudades)
                        <div class="form">
                            <label for="nombre" class="control-label">{{ $ciudades->nombre }}</label>
                            <input type="checkbox" class="form" name="ciudad" value="{{ $ciudades->id }}">    
                        </div>
                        @endforeach
                    </div>
                    <div>
                        <h2>Carrera</h2>
                            @foreach ($carrera as $carrera)
                            <div class="form">
                                <label for="nombre" class="control-label">{{ $carrera->nombre }}</label>
                                <input type="checkbox" class="form" name="area" value="{{ $carrera->id }}">    
                            </div>
                            @endforeach
                        </div>
                        
                        <br>
                        <div>
                            <button class="btn btn-primary btn-xs" type="submit">Filtrar</button>
                            <button class="btn btn-danger btn-xs" type="summit">Todas</button>
                        </div>
                    </form>   
                    </div>
                    </div>
                <div class="col-xs-8">
                        @foreach($vacantes as $vacantes)
                            <div class="col-md-10 grid-item job">
                                <a class="item-block" href="/job/{{$vacantes->id}}">
                                    <header>
                                        <img src="{{$vacantes->empresa->logo}}" alt="">
                                        <div class="hgroup">
                                            <h5>{{$vacantes->nombre}}</h5>
                                            <h5>{{$vacantes->empresa->users->name}}</h5>
                                            @if($vacantes->tipoempleo->id === 1)
                                                <span class="label label-success">{{ $vacantes->tipoempleo->nombre }}</span>
                                                @elseif($vacantes->tipoempleo->id === 2)
                                                <span class="label label-info">{{ $vacantes->tipoempleo->nombre }}</span> 
                                                @elseif($vacantes->tipoempleo->id === 3)
                                                <span class="label label-warning">{{ $vacantes->tipoempleo->nombre }}</span>
                                                @elseif($vacantes->tipoempleo->id === 4)
                                                <span class="label label-danger">{{ $vacantes->tipoempleo->nombre }}</span>
                                                @endif  
                                            </div>
                                        </header>
                                    <div class="item-body">
                                        <ul class="details">
                                            <li><i class="fa fa-map-marker"></i><span>{{$vacantes->ciudad->nombre}} - {{$vacantes->ciudad->departamento->nombre}}</span></li>
                                            <li><i class="fa fa-money"></i><span>{{$vacantes->salminimo}} - {{$vacantes->salmaximo}} Euros</span></li>
                                            <li><i class="fa fa-globe"></i><span>{{$vacantes->lenguaje->nombre}}</span></li>
                                        </ul>
                                    </div>
                                </a>
                            </div> 
                        @endforeach  
                    </div>
                </div>
            </div>
    </section>
@stop