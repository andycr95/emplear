@extends('empresa.principal')

@section('header')
    <div class="container page-name">
        <h1 class="text-center">Registro para empleadores</h1>
        <p class="lead text-center">Registrate para poder aplicar a ofertas y cursos</p>
    </div>
@stop


@section('content')
    <section>
        <div class="container">
            <form method="POST" action="{{ route('datosempre') }}" aria-label="{{ __('Register') }}" class="form-horizontal">
                    @csrf
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="telefono" class="col-md-4 control-label">Numero telefonico</label>
                                <div class="col-md-6">
                                    <input id="telefono"  type="text" class="form-control{{ $errors->has('telefono') ? ' is-invalid' : '' }}" name="telefono" value="" required autofocus>
                                        @if ($errors->has('telefono'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('telefono') }}</strong>
                                            </span>
                                        @endif
                                </div>
                            </div>
                        <div class="form-group">
                            <label for="direccion" class="col-md-4 control-label">Direccion</label>
                                <div class="col-md-6">
                                    <input id="direccion" type="text" class="form-control{{ $errors->has('direccion') ? ' is-invalid' : '' }}" name="direccion" value="{{ old('direccion') }}" required autofocus>
                                        @if ($errors->has('direccion'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('direccion') }}</strong>
                                            </span>
                                        @endif
                                </div>
                            </div>
                        <div class="form-group">
                                <label for="contacto" class="col-md-4 control-label">Contacto</label>
                                <div class="col-md-6">
                                    <input id="contacto" type="text" class="form-control{{ $errors->has('contacto') ? ' is-invalid' : '' }}" name="contacto" value="{{ old('contacto') }}" required autofocus>
                                    @if ($errors->has('contacto'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('contacto') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                        <div class="form-group">
                            <label for="web" class="col-md-4 control-label">Pagina Web</label>
                                <div class="col-md-6">
                                    <input id="web" type="text" class="form-control{{ $errors->has('web') ? ' is-invalid' : '' }}" name="web" value="{{ old('web') }}" required autofocus>
                                        @if ($errors->has('web'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('web') }}</strong>
                                            </span>
                                        @endif
                                </div>
                            </div>
                        <div class="form-group">
                            <label for="facebook" class="col-md-4 control-label">Facebook</label>
                                <div class="col-md-6">
                                    <input id="facebook" type="text" class="form-control{{ $errors->has('facebook') ? ' is-invalid' : '' }}" name="facebook" value="{{ old('facebook') }}" required autofocus>
                                        @if ($errors->has('facebook'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('facebook') }}</strong>
                                            </span>
                                        @endif
                                </div>
                            </div>
                        <div class="form-group">
                            <label for="twitter" class="col-md-4 control-label">twitter</label>
                                <div class="col-md-6">
                                    <input id="twitter" type="text" class="form-control{{ $errors->has('twitter') ? ' is-invalid' : '' }}" name="twitter" value="{{ old('twitter') }}" required autofocus>
                                        @if ($errors->has('twitter'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('twitter') }}</strong>
                                            </span>
                                        @endif
                                </div>
                            </div>
                        <div class="form-group">
                            <label for="nit" class="col-md-4 control-label">Nit</label>
                                <div class="col-md-6">
                                    <input id="nit" type="text" class="form-control{{ $errors->has('nit') ? ' is-invalid' : '' }}" name="nit" value="{{ old('nit') }}" required autofocus>
                                        @if ($errors->has('nit'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('nit') }}</strong>
                                            </span>
                                        @endif
                                </div>
                            </div>
                        <div class="form-group">
                            <label for="ciudad_id" class="col-md-4 control-label" name="ciudad_id">Ciudad</label>
                                <div class="col-md-6">
                                    <select name="ciudad_id" class="form-control">
                                        <option class="form-inputs" name="" value="#">Seleccione una opcion</option>
                                            @foreach($ciudad as $ciudad)
                                                <option  id="ciudad_id" class="form-inputs" name="ciudad_id" value="{{ $ciudad->id }}" >{{$ciudad->nombre}}</option>
                                            @endforeach
                                    </select>
                                </div>
                            </div>
                        <div class="form-group">
                            <form action="/file-upload" class="dropzone" id="my-awesome-dropzone">
                                <input type="file" name="file" />
                            </form>
                            <div>
                    </div>
                </div>
                <hr />

                <div class="row">
                    <div class="col-md-12">
                        <div id="field_descripcion" class="form-group">
                            <label for="descripcion" class="control-label">Hablanos mas de ti.</label>
                                <div class="controls">
                                    <textarea id="descripcion" class="form-control" name="descripcion" cols="50" rows="10"></textarea>
                                </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12 text-center">
                    <button type="submit" id="users_id" name="users_id" class="btn btn-primary" value="{{auth::user()->id}}" >Guardar</button>
                </div>
            </form>
        </div>
    </section>
@stop


