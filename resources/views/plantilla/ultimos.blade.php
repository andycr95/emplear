

<section class="bg-img text-center" style="background-image: url(/images/banner2.jpg)">
    <div class="container">
        <h2><strong>Últimos desarrolladores registrados</strong></h2>
        <h6 class="font-alt">Estos son algunos de los talentos registrados en nuestra plataforma</h6>
        <div class="row">
            @foreach($datos as $datos)
            <div class="col-md-3">
                <div class="thumbnail">
                    <img src="{{$datos->foto}}" alt="" class="profile-pic-list">   
                    <div class="">
                        <h6>{{$datos->users->name}}</h6>
                        <a href="" class="btn btn-primary">Ver Perfil</a>
                        <br>
                    </div>    
                </div>    
            </div>
            @endforeach
        </div>
    </div>
</section>