<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class categoria extends Model
{
    protected $table = 'categoria';
    protected $fillable = ['nombre', 'estado'];

    public function vacantes()
    {
        return $this->hasMany(vacantes::class);
    }
}
