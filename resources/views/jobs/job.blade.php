@extends('jobs.principal')

@section('header')
    <header class="page-header bg-img size-lg" style="background-image: url(/images/header-bg.jpg)">
        <div class="container header">
            <div class="header-detail">
                <a href="/empresa/perfil/{{$vacantes->empresa->users->id}}"><img class="logo" src="{{$vacantes->empresa->logo}}" alt=""></a>
                <div class="hgroup">
                    <h1>{{$vacantes->nombre}}</h1>
                    <a href="/empresa/perfil/{{$vacantes->empresa->users->id}}"><h3>{{$vacantes->empresa->users->name}}</h3></a>
                </div>
                <hr>
                    <ul class="details cols-3">
                        <li><li class="fa fa-clock-o"></i><span> hace {{($vacantes->created_at)->format('d')}} días</span></li>
                        <li><i class="fa fa-map-marker"></i><span>{{$vacantes->ciudad->nombre}}</span></li>
                        <li><i class="fa fa-briefcase"></i><span>{{$vacantes->tipoempleo->nombre}}</span></li>
                        <li><i class="fa fa-money"></i><span>{{$vacantes->salminimo}} - {{$vacantes->salminimo}} Mes</span></li>
                        <li><i class="fa fa-globe"></i><span>{{$vacantes->lenguaje->nombre}}</span></li>
                        <li><i class="fa fa-tags"></i><span>{{$vacantes->palabrasclave}}</span></li>
                    </ul>
                <div class="button-group">
                    <ul class="social-icons">
                        <li class="title">Comparte esta oferta</li>
                    </ul>
                    <div class="addthis_inline_share_toolbox"></div>
                </div>
            </div>
        </div>
    </header>
@stop
@section('content')
    <section>
    @if (session()->has('info'))
    <div class="alert alert-success">
        {{ session('info') }}
    </div>
    @endif
        <div class="container">
            <h4>Descripción de la oferta</h4>
                <p>{{$vacantes->descripcion}}</p>
                <br>
            <h4>Rango de edad</h4>
                <div>
                    <p>{{$vacantes->rangoedad}}</p>
                </div>
                <br>
            <h4>Fecha de contratacion</h4>
                <div>
                    <p>{{$vacantes->fecha}}</p>
                </div>
                <br>
            <h4>Vence</h4>
                <div>
                    <p>{{$vacantes->vence}}</p>
                </div>
                <br>
            <h4>Como comunicar / aplicar a la oferta</h4>
                <div>
                    <p>Interesados enviar C.V. a {{$vacantes->empresa->users->email}} </p>
                </div>
                <br>
            <h4>Acerca de la compañía</h4>
                <div>
                    <strong>{{$vacantes->empresa->users->name}}:</strong><br />
                    <p><strong>{{$vacantes->empresa->users->name}}</strong> {{$vacantes->empresa->descripcion}}</p>
                <div>
            @guest    
                <div class="col-md-12 text-center">
                    <a href="{{ route('login')}}"><button type="submit" class="btn btn-primary"  >Aplicar</button></a>
                </div> 
            @else
                <form method="POST" action="{{ route('apli') }}" aria-label="{{ __('Register') }}" class="form-horizontal">
                    @csrf
                        <div class="col-md-12 text-center">
                            <input type="hidden" id="vacantes_id" name="vacantes_id" value="{{$vacantes->id}}"></input>
                            <input type="hidden" id="empresa_id" name="empresa_id" value="{{$vacantes->empresa_id}}"></input>
                            <button type="submit" id="users_id" name="users_id" class="btn btn-primary" value="{{auth::user()->id}}" >Aplicar</button>
                        </div>
                    </form>
            @endguest
        </div>
    </section>
@stop
