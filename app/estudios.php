<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\users;
use App\estado;
use App\nivel;
use App\ciudad;
use App\anio;

class estudios extends Model
{
    protected $table = 'estudios';
    protected $fillable = ['id','institucion','enfasis','nivel_id','ciudad_id','users_id','anio_id','estado_id'];

    public function nivel()
    {
        return $this->belongsTo(nivel::class);
    }

    public function ciudad()
    {
        return $this->belongsTo(ciudad::class);
    }

    public function anio()
    {
        return $this->belongsTo(anio::class);
    }

    public function estado()
    {
        return $this->belongsTo(estado::class);
    }

    public function users(){
        return $this->belongsToMany(users::class);
    }

   
    
}
