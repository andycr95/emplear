<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\users;
use App\ciudad;
use App\categoria;
use App\tipoempleo;

class empleo extends Model
{
    protected $fillable = ['titulo','nombre','telefono','ocupacion', 'fecha_inicio','fecha_fin','empresa','users_id', 'tipoempleo_id','ciudad_id', 'categoria_id'];

    
    public function users(){
        return $this->belongsTo(users::class);
    }

    public function ciudad(){
        return $this->belongsTo(ciudad::class);
    }

    public function categoria(){
        return $this->belongsTo(categoria::class);
    }

    public function tipoempleo(){
        return $this->belongsTo(tipoempleo::class);
    }
}
