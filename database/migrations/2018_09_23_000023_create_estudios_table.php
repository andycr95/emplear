<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEstudiosTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'estudios';

    /**
     * Run the migrations.
     * @table estudios
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('institucion', 45)->nullable()->default(null);
            $table->string('enfasis', 45)->nullable()->default(null);
            $table->unsignedInteger('nivel_id');
            $table->unsignedInteger('estado_id');
            $table->unsignedInteger('ciudad_id');
            $table->unsignedInteger('anio_id');
            $table->unsignedInteger('users_id');

            $table->index(["nivel_id"], 'estudios_nivel_id_foreign');

            $table->index(["estado_id"], 'estudios_estado_id_foreign');

            $table->index(["ciudad_id"], 'estudios_ciudad_id_foreign');

            $table->index(["users_id"], 'estudios_users_id_foreign');

            $table->index(["anio_id"], 'estudios_anio_id_foreign');
            $table->timestamps();


            $table->foreign('anio_id', 'estudios_anio_id_foreign')
                ->references('id')->on('anio')
                ->onDelete('restrict')
                ->onUpdate('restrict');

            $table->foreign('ciudad_id', 'estudios_ciudad_id_foreign')
                ->references('id')->on('ciudad')
                ->onDelete('restrict')
                ->onUpdate('restrict');

            $table->foreign('estado_id', 'estudios_estado_id_foreign')
                ->references('id')->on('estado')
                ->onDelete('restrict')
                ->onUpdate('restrict');

            $table->foreign('nivel_id', 'estudios_nivel_id_foreign')
                ->references('id')->on('nivel')
                ->onDelete('restrict')
                ->onUpdate('restrict');

            $table->foreign('users_id', 'estudios_users_id_foreign')
                ->references('id')->on('users')
                ->onDelete('restrict')
                ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
