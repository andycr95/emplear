@extends('principal')

@section('header')
    <div class="container page-name">
        <h1 class="text-center">Registro para Empresa</h1>
        <p class="lead text-center">Registrase para poder publicar ofertas</p>
    </div>
@endsection

@section('content')
    <section>
        <div class="container">
            <form method="POST" action="{{ route('registerempres') }}" aria-label="{{ __('Register') }}" class="form-horizontal">
                    @csrf
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="name" class="col-md-4 control-label">Nombre de la compañia</label>
                                <div class="col-md-6">
                                    <input id="name" id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" required autofocus>
                                        @if ($errors->has('name'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('name') }}</strong>
                                            </span>
                                            @endif
                                </div>
                            </div>
                            <div class="form-group">
                                    <label for="apellidos" class="col-md-4 control-label">Razón social registrada en DIAN</label>
                                        <div class="col-md-6">
                                            <input id="apellidos"  type="text" class="form-control" name="apellidos" value="" required autofocus>
                                                @if ($errors->has('apellidos'))
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $errors->first('apellidos') }}</strong>
                                                    </span>
                                                    @endif
                                        </div>
                                    </div>
                        <div class="form-group">
                            <label for="email" class="col-md-4 control-label">{{ __('E-Mail Address') }}</label>
                                <div class="col-md-6">
                                    <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>
                                    @if ($errors->has('email'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>


                        <div class="form-group">
                            <label for="password" class="col-md-4 control-label">{{ __('Password') }}</label>
                                <div class="col-md-6">
                                    <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>
                                    @if ($errors->has('password'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                        <div class="form-group">
                            <label for="password-confirm" class="col-md-4 control-label">{{ __('Confirm Password') }}</label>
                                <div class="col-md-6">
                                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                                </div>
                            </div>
                    </div>
                </div>
                <hr />

                <div class="row">
                    <div class="col-md-12 text-center text-info"><br />
                        Cuando te registras tambien te suscribes a la lista de correos para notificaciones
                    </div>
                    </div>
                <hr />
                <div class="row">
                    <div class="form-group">
                        <div class="col-md-12 text-center">
                           <button type="submit" class="btn btn-primary">Registrar</button>
                        </div>
                    </div>
                    </div>
            </form>
        </div>
    </section>
@endsection
