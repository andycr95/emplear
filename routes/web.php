<?php


Route::get('/', 'vacantesController@index')->name('home');


Auth::routes();

/* Vacantes */
Route::get('/jobb', 'vacantesController@index');
Route::get('/job/{id}', 'vacantesController@show');
Route::get('/jobs', 'vacantesController@index3')->name('jobs');
Route::get('/vacante/{id}', 'vacantesController@index2')->name('vacante');

        
/* Datos usuarios */
Route::get('/hojav/{id}', 'datospController@index2');
Route::post('/hojav', 'datospController@store')->name('hojav');
Route::post('/hojavc', 'datospController@store1');
Route::post('/hojavp', 'datospController@store2');
Route::post('/hojavs', 'datospController@store3');
Route::post('/hojave', 'datospController@store4');

/* Supra admin */
Route::get('/admin', 'adminController@index');
Route::post('/admin/rolecreate/{id}', 'adminController@rolestore')->name('admin.role.store');
Route::get('/admin/empleos/categorias', 'categoriaController@index');
Route::post('/admin/empleos/categorias/store', 'categoriaController@store')->name('admin.categoria.create');


/* Usuarios */
Route::get('/admin/users/', 'adminController@usuariosindex');
Route::get('/admin/usershow/{id}', 'adminController@usershow');
Route::get('/user/perfil/{id}', 'usersController@perfil2');
Route::get('/plantilla/ultimos', 'datospController@index');

/* Empresas */
Route::get('/admin/empresas', 'adminController@empresaindex');
Route::get('/admin/empresashow/{id}', 'adminController@empreshow');
Route::get('/admin/empresashow/vacante/{id}', 'adminController@vacante');
Route::get('/empresas', 'empresaController@index');
Route::get('/users/perfil/{id}', 'empresaController@perfil');
Route::get('/empresa/datos/{id}', 'empresaController@index2');
Route::post('/vacantecreate', 'vacantesController@store')->name('vacantecreate');
Route::get('/empresa/register', 'Auth\RegisterController@showRegistrationForm2')->name('registerempre');
Route::post('/empresa/register', 'Auth\RegisterEmpreController@register')->name('registerempres');
Route::get('/empresa/perfil/{id}', 'empresaController@perfil2');
Route::post('/empresa/datos', 'empresaController@store')->name('datosempre');

/* Aplicantes y aplicaciones */
Route::post('/admin/aplicante', 'adminController@aplicantstore')->name('admin.aplicant.store');
Route::post('/job', 'aplicacionController@store')->name('apli');
Route::get('/publicaciones/{id}', 'aplicacionController@index2');
Route::get('/aplicantes/{id}', 'aplicantesController@index');

/* Estados */
Route::post('/admin/editestado/{id}', 'adminController@editestado')->name('admin.estado.store');
Route::post('/admin/empresaesta/{id}', 'adminController@empresaesta')->name('admin.emprees.store');

/* candidatos */
Route::get('/candidato/perfil/{id}', 'usersController@perfil');
Route::get('/candidatos', 'usersController@index')->name('candidatos');

/* Roles */
Route::get('/continuar', 'rolesController@index');
Route::get('/continuaremp', 'rolesController@index2');
Route::post('/Bienvenido', 'rolesController@store');

/* Social */
Route::get('login/facebook', 'SocialController@redirectToProvider')->name('facebook');
Route::get('login/facebook/callback', 'SocialController@handleProviderCallback');