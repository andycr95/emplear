<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Socialite;

class SocialController extends Controller
{


    public function redirectToProvider()
    {
        return Socialite::driver('FACEBOOK')->redirect();
    }

    public function handleProviderCallback()
    {
        $user = Socialite::driver('FACEBOOK')->user();
        return ($user->getAvatar());

        // $user->token;
    }
}
