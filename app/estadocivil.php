<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class estadocivil extends Model
{
    protected $table = 'estadocivil';
    protected $fillable = ['nombre', 'estado'];

    public function datosp()
    {
        return $this->hasMany(datosp::class);
    }
}
