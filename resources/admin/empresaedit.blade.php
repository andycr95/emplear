@extends('admin.admin')
@section('content')
    <h1>Editar</h1>
        <div class="box">
            <div class="box-header">
              <h3 class="box-title">{{ $datos->users->name }}</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
              <table class="table table-hover">
                <tbody>
                  <tr>
                    <th>ID</th>
                    <th>Nombre</th>
                    <th>Estado</th>
                    <th>Numero de vacantes</th>
                  </tr>
                  <tr>  
                    <td>{{ $datos->id }}</td>
                    <td>{{ $datos->users->name }}</td>
                    @if ($datos->estado == 'Activo')
                    <td><span class="label label-success">{{ $datos->estado }}</span></td>
                    @else
                    <td><span class="label label-danger">{{ $datos->estado }}</span></td>
                    @endif          
                    <td>{{ $datos->vacantes->count() }}</td>                  
                  </tr>
                </tbody>
              </table>  
            </div>
          </div>
        <div class="row">
            <div class="col-md-4">
                <div class="box">
                    <form method="POST" action="{{ route('admin.emprees.store', $datos->id) }}" aria-label="contacto" class="form-horizontal"> 
                        @csrf
                            <div class="box-header with-border">
                                <h3 class="box-title">Cambiar Estado</h3>
                            </div>
                            <div class="box-body">
                                <table class="table table-bordered">
                                    <tbody>
                                        @if (session()->has('info'))
                                            <div class="alert alert-success">
                                                {{ session('info') }}
                                            </div>
                                        @endif
                                        <tr>
                                            <th>Estado</th>
                                        </tr>
                                        <tr>
                                            <td>
                                                <input type="checkbox" class="form" name="estado" id="estado" value="Activo"><span class="label label-success">Activo</span>
                                                <input type="checkbox" class="form" name="estado" id="estado" value="Inactivo"><span class="label label-danger">Inactivo</span>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.box-body -->
                            <div class="box-footer clearfix">      
                                <button type="submit">Guardar</button>
                            </div>
                    </form> 
                </div>
            </div>
            <div class="col-md-8">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Vacantes</h3>
                    </div>
                    <div class="box-body">
                        <table class="table table-bordered">
                            <tbody>
                                <tr>
                                    <th>Nombre</th>
                                    <th>Rango de edad</th>
                                    <th>Numero de aplicantes</th>
                                    <th>Estado</th>
                                </tr>
                                @foreach ($v as $v)
                                <tr>
                                    <td><a href="/admin/empresashow/vacante/{{ $v->id }}">{{ $v->nombre }}</a></td>
                                    <td>{{ $v->rangoedad }}</td>
                                    <td>{{ $v->aplicaciones->count() }}</td>
                                    <td>{{ $v->estado }}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="box-footer clearfix">      
                    </div>
                </div>
            </div>
        </div>
@endsection