<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\ciudad;

class departamento extends Model
{
    protected $table = 'departamento';
    protected $fillable = ['nombre', 'estado'];

    public function ciudad()
    {
        return $this->hasMany(ciudad::class);
    }
}
