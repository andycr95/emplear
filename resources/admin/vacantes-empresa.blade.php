@extends('admin.admin')
    @section('content')
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Filtro de aplicaciones</h3>
                        <div class="box-tools">
                            <form action="/admin">
                                <div class="input-group input-group-sm" style="width: 150px;">
                                    <div class="input-keyword">
                                        <input type="text" name="search" class="form-control pull-right" placeholder="Search">
                                    </div>
                                    <div class="input-group-btn">    
                                        <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                <form method="POST" action="{{ route('admin.aplicant.store') }}" aria-label="contacto" class="form-horizontal"> 
                    @csrf
                    <div class="box-body table-responsive no-padding">
                        <table class="table table-hover">
                            <tbody>
                                @if (session()->has('info'))
                                    <div class="alert alert-success">
                                        {{ session('info') }}
                                    </div>
                                @endif
                                <tr>
                                    <th>ID</th>
                                    <th>Nombre</th>
                                    <th>Direccion</th>
                                    <th>Profesion</th>
                                    <th>Años de experiencia</th>
                                    <th>Estado</th>
                                    <th>Apto</th>
                                </tr>
                                @foreach ($vacante as $vacantes)        
                                <tr>  
                                    <td>{{ $vacantes->id }}</td>
                                    <td><a href="/admin/empresashow/{{$vacantes->users->id}}">{{ $vacantes->users->name }}</a></td>
                                    <td>--</td>
                                    <td>--</td>
                                    <td>--</td>
                                    @if ($vacantes->aplicante != '')
                                        @if ($vacantes->aplicante->estado == 'Aprobado')
                                            <td><span class="label label-success">{{ $vacantes->aplicante->estado }}</span></td> 
                                        @else
                                            <td><span class="label label-danger">En estudio</span></td>
                                        @endif
                                    @else
                                        <td><span class="label label-danger">En estudio</span></td>
                                    @endif
                                    <td>
                                        <input type="checkbox" value="{{$vacantes->users_id}}" name="users_id">
                                        <input type="hidden" value="{{$vacantes->empresa_id}}" name="empresa_id">
                                        <input type="hidden"  value="{{ $vacantes->id }}" name="aplicacion_id">
                                        <input type="hidden"  value="Aprobado"name="estado">
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="box-footer clearfix">      
                        <button class="pull-right" type="submit">Guardar</button>
                    </div>
                </form> 
                </div>
                </div>
            </div>  
            <!-- right col -->
        </div>
        <!-- /.row (main row) -->
        @endsection