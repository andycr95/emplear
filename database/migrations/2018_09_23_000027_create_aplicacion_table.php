<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAplicacionTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'aplicacion';

    /**
     * Run the migrations.
     * @table aplicacion
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('vacantes_id');
            $table->string('estado', 25);
            $table->unsignedInteger('users_id');
            $table->unsignedInteger('empresa_id');

            $table->index(["vacantes_id"], 'aplicacion_vacantes_id_foreign');

            $table->index(["empresa_id"], 'aplicacion_empresa_id_foreign');

            $table->index(["users_id"], 'aplicacion_users_id_foreign');
            $table->timestamps();


            $table->foreign('empresa_id', 'aplicacion_empresa_id_foreign')
                ->references('id')->on('empresa')
                ->onDelete('restrict')
                ->onUpdate('restrict');

            $table->foreign('users_id', 'aplicacion_users_id_foreign')
                ->references('id')->on('users')
                ->onDelete('restrict')
                ->onUpdate('restrict');

            $table->foreign('vacantes_id', 'aplicacion_vacantes_id_foreign')
                ->references('id')->on('vacantes')
                ->onDelete('restrict')
                ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
