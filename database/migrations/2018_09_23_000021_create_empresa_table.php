<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmpresaTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'empresa';

    /**
     * Run the migrations.
     * @table empresa
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('nit');
            $table->string('direccion', 60)->nullable()->default(null);
            $table->string('telefono', 45)->nullable()->default(null);
            $table->string('contacto', 45)->nullable()->default(null);
            $table->unsignedInteger('ciudad_id');
            $table->string('descripcion')->nullable()->default(null);
            $table->unsignedInteger('users_id');
            $table->string('web', 45)->nullable()->default(null);
            $table->string('twitter', 45)->nullable()->default(null);
            $table->string('facebook', 45)->nullable()->default(null);
            $table->string('estado', 45)->nullable()->default(null);
            $table->string('logo')->nullable()->default(null);
            $table->string('remember_token', 200)->nullable()->default(null);

            $table->index(["users_id"], 'empresa_users_id_foreing');

            $table->index(["ciudad_id"], 'empresa_ciudad_id_foreign');

            $table->unique(["nit"], 'empresa_nit_unique');
            $table->timestamps();


            $table->foreign('ciudad_id', 'empresa_ciudad_id_foreign')
                ->references('id')->on('ciudad')
                ->onDelete('restrict')
                ->onUpdate('restrict');

            $table->foreign('users_id', 'empresa_users_id_foreing')
                ->references('id')->on('users')
                ->onDelete('restrict')
                ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
