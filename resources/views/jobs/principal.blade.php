
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>{{ config('app.name') }}</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="Busca y/o publica ofertas laborales para desarrolladores php y Laravel.">
        <meta property="og:title" content={{ config('app.name') }}>
        <meta property="og:description" content="Busca y/o publica ofertas laborales.">
        <meta property="og:image" content="img/header-bg.jpg">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <link rel="icon" href="/img/favicon.png">
        <link href='https://fonts.googleapis.com/css?family=Oswald:100,300,400,500,600,800%7COpen+Sans:300,400,500,600,700,800%7CMontserrat:400,700' rel='stylesheet' type='text/css'>
        <link href='https://fonts.googleapis.com/css?family=Oswald:100,300,400,500,600,800%7COpen+Sans:300,400,500,600,700,800%7CMontserrat:400,700' rel='stylesheet' type='text/css'>
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
        <link href="/css/app2.css" rel="stylesheet">
    </head>
    <body class=" smart-nav" id="app">
        <nav class="navbar  navbar-expand-md fixed-top ">
            <div class="container">
                <div class="pull-left">
                    <a type="button" class="navbar-toggle" data-toggle="collapse" data-target="collapse"><i class="ti-menu"></i></a>
                    <div class="logo-wrapper">
                            <a class="logo" href="/"><img src="/img/logo3.png" alt="{{ config('app.name') }} logo"></a>
                            <a class="logo-alt" href="/"><img src="/img/logo4.png" alt="{{ config('app.name') }} logo"></a>
                     </div>
                </div>
                
                @guest
                    <div class="pull-right user-login">
                        <a class="btn btn-sm btn-primary" href="{{ route('login') }}">Login</a>
                    </div>    
                    <ul class="nav-menu" id="collapse">
                        <li class="nav-item"><a href="/">Home</a></li>
                        <li class="nav-item"><a href="/jobs">Ofertas laborales</a></li>
                    </ul>
                </div>
                @else
                    <ul class="nav-menu">
                        @if(auth()->user()->hasRole('empresa'))
                            <li class="nav-item"><a href="/">Home</a></li>
                            <li class="nav-item"><a href="/candidatos">Candidatos</a></li>
                            <li class="nav-item"><a href="/empresa/datos/{{auth::user()->id}}">Complete sus datos</a></li>
                            <li class="nav-item"><a href="/vacante/{{Auth::user()->id}}">Publique vacantes</a></li>
                            <li class="nav-item"><a href="/aplicantes/{{Auth::user()->id}}">Aplicantes</a></li>
                            <li class="nav-item dropdown"><a href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" >{{ Auth::user()->name }} <span class="caret"></span></a>
                                    <ul class="dropdown-menu">
                                        <li><a class="dropdown-item"  href="/publicaciones/{{Auth::user()->id}}">Mis publicaciones</a></li>
                                        <li><a class="dropdown-item"  href="/empresa/perfil/{{auth::user()->id}}">Perfil</a></li>
                                        <li><a class="dropdown-item" href="logout" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">{{ __('Logout') }}</a></li>
                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            @csrf
                                        </form>
                                    </ul>
                            </li>
                        @elseif(auth()->user()->hasRole('candidato'))
                            <li class="nav-item"><a href="/">Home</a></li>
                            <li class="nav-item"><a href="/jobs">Ofertas laborales</a></li>
                            <li class="nav-item"><a href="/empresas">Empresas</a></li>
                            <li class="nav-item" ><a href="/hojav/{{auth::user()->id}}">Datos personales</a></li>
                            <li class="nav-item dropdown"><a role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" >{{ Auth::user()->name }} <span class="caret"></span></a>
                                <ul class="dropdown">
                                    <li><a class="dropdown-item"  href="/candidato/perfil/{{auth::user()->id}}">Perfil</a></li>
                                    <li><a class="dropdown-item" href="logout" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">{{ __('Logout') }}</a></li>
                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            @csrf
                                        </form>
                                </ul>
                            </li>
                        @elseif(auth()->user()->hasRole('admin'))
                            <li class="nav-item"><a href="/">Home</a></li>
                            <li class="nav-item"><a href="/admin">Dashboard</a></li>
                            <li class="nav-item"><a href="/hojav/{{auth::user()->id}}">Datos personales</a></li>
                            <li class="nav-item dropdown"><a href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" >{{ Auth::user()->name }} <span class="caret"></span></a>
                                <ul class="dropdown-menu">
                                    <li><a class="dropdown-item" href="logout" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">{{ __('Logout') }}</a></li>
                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            @csrf
                                        </form>
                                </ul>
                            </li>
                        @endif
                    </ul>
                @endguest
                
                </div>
            </div>
            </nav>             
        
            @yield('header')
            <main>

            @yield('content')

            </main>

        <footer class="site-footer">
            <div class="container">
                <div class="row" id="about">
                    <div class="col-xs-6 col-md-4">
                        <div class="single-footer-widget mail-chimp">
                            <h6 class="mb-15">Instragram Feed</h6>
                            <ul class="instafeed d-flex flex-wrap">
                            </ul>
                        </div>
                    </div>
                    <div class="col-xs-6 col-md-4">
                        <h6>Personas</h6>
                        <ul class="footer-nav">
                            <li><a href="/contactos">Contactos para personas</a></li>
                            <li><a href="/jobs">Buscar empleo</a></li>
                        </ul>
                    </div>
                    <div class="col-xs-6 col-md-4">
                        <h6>Reclutadores</h6>
                        <ul class="footer-nav">
                            <li><a href="/contactos">Contactos para reclutadores</a></li>
                            <li><a href="/jobs">Buscar personas</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <br>
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-sm-6 col-xs-12">
                        <p class="copyright-text">© 2018 OFERTAS LABORALES | <a href="/" target="_blank">{{ config('app.name') }}</a> | COLOMBIA — ALL RIGHTS RESERVED</p>
                    </div>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <ul class="social-icons">
                            <li><a class="twitter" href="https://twitter.com/" target="_blank"><i class="fa fa-twitter"></i></a></li>
                            <li><a class="twitter" href="https://facebook.com/" target="_blank"><i class="fa fa-facebook"></i></a></li>
                         </ul>
                    </div>
                </div>
            </div>
            <form method="POST" action="" accept-charset="UTF-8" id="form-delete"><input name="_method" type="hidden" value="DELETE"><input name="_token" type="hidden" value="JkXa4dJUwvaXI2zg76c9nwzNoBty2fHh0B4ESJwg">
            </form>
            </footer>
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>   
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
        <script src="/js/plantilla.js"></script>  
        <script src="/js/vue.js"></script>  
        <script>
                $(document).ready(function() {

                    //set up the 'Add New Question' button
                    $('#addQuestion').click(function(e){
                        //prevent the button from submitting the form
                        e.preventDefault();

                        //get the new question number
                        var questionNumber = $('.question').length + 1;

                        //clone the master questionChanger and 'type1' question (we need to remove the ID from the questionType - we don't need it)
                        var questionChanger = $('#masterQuestion .questionChanger').clone(true);
                        var questionType = $('#type1').clone().removeAttr('id');

                        //create a new wrapper for the new question, set the question number (used later), add a class, and add the new content to it
                        var newQuestion = $('<div>').data('qNum', questionNumber).addClass('question').append(questionChanger, questionType);

                        //now loop through the '.dynamic' elements so we can change the name
                        $('.dynamic', newQuestion).each(function() {
                            //get the old dummy name
                            var oldName = $(this).attr('name');

                            //replace the dummy text (*) with the question number
                            $(this).attr('name', oldName.replace('*', questionNumber));
                        })

                        //add the new question to the #questions DIV
                        newQuestion.appendTo('#questions');
                    });


                    //set up the 'Question Type' changer
                    $('.questionChanger select').change(function() {
                        //what type of question are we cloning (match the values of the dropdown to the IDs of the master question types)
                        var newQuestionType = $('#' + $(this).val()).clone().removeAttr('id');

                        //get the question number (this time from the data-qNum attribute)
                        var questionNumber = $(this).parents('.question').data('qNum');

                        //loop through the new question and change the name of all the '.dynamic' elements
                        $(newQuestionType).find('.dynamic').each(function(){
                            //get the old dummy name
                            var oldName = $(this).attr('name');

                            //replace the dummy text (*) with the question number (this time from the data-qNum attribute)
                            $(this).attr('name', oldName.replace('*', questionNumber));
                        })

                        //now add the new question type
                        $(this).parents('.question').find('.questionSet').html(newQuestionType);
                    });

                });
            </script>
    </body>
</html>
