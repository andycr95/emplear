<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Laravel\Scout\Searchable;
use App\empresa;
use App\categoria;
use App\ciudad;
use App\tipoempleo;
use App\aplicacion;
use App\aplicantes;
use App\tag;


class vacantes extends Model
{
    use Searchable;

    protected $table = 'vacantes';
    protected $fillable = ['nombre','palabrasclave','descripcion','salmaximo','salminimo','correo','rangoedad','cantidad','vence','empresa_id','departamento_id','categoria_id','ciudad_id','tipoempleo_id','lenguaje_id','experiencia','fecha','covacante','estado'];


    public function searchables()
    {
        return 'vacantes-index';
    }

    public function toSearchableArray()
    {
        $array = $this->toArray();

        // Customize array...

        return $array;
    }
    
    public function empresa()
    {
        return $this->belongsTo(empresa::class);
    }

    public function categoria()
    {
        return $this->belongsTo(categoria::class);
    }

    public function aplicante()
    {
        return $this->hasOne(aplicantes::class);
    }

    public function aplicaciones()
    {
        return $this->hasMany(aplicacion::class);
    }

    public function aplicacion()
    {
        return $this->hasOne(aplicacion::class);
    }

    public function ciudad()
    {
        return $this->belongsTo(ciudad::class);
    }

    public function tipoempleo()
    {
        return $this->belongsTo(tipoempleo::class);
    }

    public function lenguaje()
    {
        return $this->belongsTo(lenguaje::class);
    }
    
    
}
