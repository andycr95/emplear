<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\users;
use App\empresa;
use App\aplicacion;
use App\vacantes;


class aplicantes extends Model
{
    protected $table = 'aplicantes';
    protected $fillable = ['users_id','estado', 'empresa_id', 'aplicacion_id'];



    public function users()
    {
        return $this->belongsTo(users::class);
    }

    public function aplicacion()
    {
        return $this->belongsTo(aplicacion::class);
    }

    public function empresa()
    {
        return $this->belongsTo(empresa::class);
    }

    public function vacantes()
    {
        return $this->belongsTo(vacantes::class);
    }


}
