@extends('principal')


@section('header')
    <div class="container page-name">
        <h1 class="text-center">Ingresa a tu cuenta</h1>
        <p class="lead text-center">Podrás utilizar los servicios de nuestro sitio</p>
    </div>
@stop

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-3 text-center">
                <h2><b>Emplear</b>Login</a></h2>
            </div>
        </div>
        <hr />
        <div class="row">
            <div class="col-md-6">
                <h4 class="text-center">Si ya tienes cuenta ingresa acá</h4>
                <form method="POST" action="{{ route('login') }}" aria-label="{{ __('Login') }}">
                        @csrf
                    <div class="form-group {{ $errors->has('email') ? ' is-invalid' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-Mail</label>
                            <div class="col-md-6">
                                    <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>

                                    @if ($errors->has('email'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                    @endif
                            </div>
                        </div>
                    <div class="form-group {{ $errors->has('password') ? 'is-invalid' : '' }}">
                            <label for="password" class="col-md-4 control-label">Contraseña</label>
                            <div class="col-md-6">
                                    <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                    @if ($errors->has('password'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                    @endif
                            </div>
                        </div>
                    <div class="form-group">
                        <div class="col-md-8 col-md-offset-4">
                            <div class="checkbox">
                                <label><input type="checkbox" name="remember"> Recordarme</label> |
                                <a href="{{ route('password.request') }}"> Olvidó su contraseña?</a>
                            </div>
                        </div>
                        </div>
                    <div class="form-group rigth">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary">{{ __('Login') }}</button>
                                <br /><br /><br /><br />
                             </div>
                        </div>
                    <div class="form-group">
                        <div class="col-md-8 col-md-offset-4">
                                <a href="{{ route('facebook') }}"> Facebook</a>
                        </div>
                        </div>
                </form>
            </div>
            <div class="col-md-6 text-center">
                    <h1>Registrate</h1>
                    <h3>Si aún no tienes cuenta</h3>
                    
                    <div>
                    <h3>Empleado</h3>
                <a href="{{route('register')}}" class="btn btn-primary"><i class="fa fa-building"></i> Registrate</a>
                </div>
            <br/><br/>
            <h3>Empresa</h3>
                <a href="{{route('registerempre')}}" class="btn btn-primary"><i class="fa fa-building"></i> Registrate</a>
            </div>
            </div>
        </div>
        <br/><br/><br/>
        <hr/>
@endsection
