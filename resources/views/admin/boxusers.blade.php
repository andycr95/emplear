@extends('admin.admin')
@section('content')
  <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div class="box-header">
            <h3 class="box-title">Usuarios</h3>
            
            <div class="box-tools">
              <form action="/admin">
                <div class="input-group input-group-sm" style="width: 150px;">
                  
                  <div class="input-keyword">
                    <input type="text" name="search" class="form-control pull-right" placeholder="Search">
                  </div>
                  <div class="input-group-btn">
                    
                    <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                  </div>
                  
                </div>
              </form>
            </div>
          </div>
          <!-- /.box-header -->
          <div class="box-body table-responsive no-padding">
            <table class="table table-hover">
              <tbody><tr>
                <th>ID</th>
                <th>Nombre</th>
                <th>Email</th>
                <th>Estado</th>
                <th>Role</th>
                <th>Datos personales</th>
              </tr>
              <tr>
                
                @foreach ($users as $user)
                
                <tr>  
                  <td>{{ $user->id }}</td>
                  <td><a href="/admin/usershow/{{$user->id}}">{{ $user->name }}</a></td>
                  <td>{{ $user->email }}</td>
                  @if ($user->estado == 'activo')
                  <td><span class="label label-success">{{ $user->estado }}</span></td>
                  @else
                  <td><span class="label label-danger">{{ $user->estado }}</span></td>
                  @endif
                  <td>{{ $user->roles->pluck('displayname')->implode(',') }}</td> 
                  @if ($user->datosp == '')
                  <td>Datos no completados</td>         
                  @else
                  <td><a href="/admin/usershow/{{ $user->id }}">ir</a></td> 
                  @endif
                </tr>
                @endforeach
              </tbody>
            </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
      </div>  
    <!-- right col -->
  </div>
  <!-- /.row (main row) -->
@endsection