<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\licencia;

class licencia extends Model
{
    protected $table = 'licencia';
    protected $fillable = ['nombre', 'estado'];

    public function datosp()
    {
        return $this->hasMany(datosp::class);
    }

    
}
