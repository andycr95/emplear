<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVacantesTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'vacantes';

    /**
     * Run the migrations.
     * @table vacantes
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('nombre')->nullable()->default(null);
            $table->string('palabrasclave')->nullable()->default(null);
            $table->string('descripcion')->nullable()->default(null);
            $table->string('salmaximo')->nullable()->default(null);
            $table->string('salminimo')->nullable()->default(null);
            $table->string('email', 45)->nullable()->default(null);
            $table->string('rangoedad', 11)->nullable()->default(null);
            $table->integer('cantidad')->nullable()->default(null);
            $table->date('vence')->nullable()->default(null);
            $table->unsignedInteger('empresa_id');
            $table->unsignedInteger('categoria_id');
            $table->unsignedInteger('ciudad_id');
            $table->unsignedInteger('tipoempleo_id');
            $table->unsignedInteger('lenguaje_id');
            $table->string('experiencia', 11)->nullable()->default(null);
            $table->date('fecha')->nullable()->default(null);
            $table->integer('convocante')->nullable()->default(null);
            $table->string('estado', 45)->nullable()->default(null);

            $table->index(["tipoempleo_id"], 'vacantes_tipoempleo_id_foreign');

            $table->index(["empresa_id"], 'vacantes_empresa_id_foreign');

            $table->index(["categoria_id"], 'vacantes_categoria_id_foreign');

            $table->index(["lenguaje_id"], 'vacantes_lenguaje_id_foreign');

            $table->index(["ciudad_id"], 'vacantes_ciudad_id_foreign');
            $table->nullableTimestamps();


            $table->foreign('categoria_id', 'vacantes_categoria_id_foreign')
                ->references('id')->on('categoria')
                ->onDelete('restrict')
                ->onUpdate('restrict');

            $table->foreign('ciudad_id', 'vacantes_ciudad_id_foreign')
                ->references('id')->on('ciudad')
                ->onDelete('restrict')
                ->onUpdate('restrict');

            $table->foreign('empresa_id', 'vacantes_empresa_id_foreign')
                ->references('users_id')->on('empresa')
                ->onDelete('restrict')
                ->onUpdate('restrict');

            $table->foreign('lenguaje_id', 'vacantes_lenguaje_id_foreign')
                ->references('id')->on('lenguaje')
                ->onDelete('restrict')
                ->onUpdate('restrict');

            $table->foreign('tipoempleo_id', 'vacantes_tipoempleo_id_foreign')
                ->references('id')->on('tipoempleo')
                ->onDelete('restrict')
                ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
