<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDatospTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'datosp';

    /**
     * Run the migrations.
     * @table datosp
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('documento');
            $table->date('fecha_nacimiento')->nullable()->default(null);
            $table->string('direccion', 45)->nullable()->default(null);
            $table->string('descripcion')->nullable()->default(null);
            $table->integer('telefono')->nullable()->default(null);
            $table->string('años_labor', 11)->nullable()->default(null);
            $table->string('numero_licencia', 45)->nullable()->default(null);
            $table->unsignedInteger('users_id');
            $table->unsignedInteger('tipodocumento_id');
            $table->unsignedInteger('ciudad_id');
            $table->unsignedInteger('estadocivil_id');
            $table->unsignedInteger('areas_id');
            $table->unsignedInteger('genero_id');
            $table->unsignedInteger('licencia_id');
            $table->string('estado', 45)->nullable()->default(null);
            $table->string('foto', 200);

            $table->index(["licencia_id"], 'datosperso_licencia_id_foreign');

            $table->index(["tipodocumento_id"], 'datosperso_tipodocumento_id_foreign');

            $table->index(["ciudad_id"], 'datosperso_ciudad_id_foreign');

            $table->index(["estadocivil_id"], 'datosperso_estadocivil_id_foreign');

            $table->index(["users_id"], 'datosperso_users_id_foreign');

            $table->index(["areas_id"], 'datosperso_areas_id_foreign');

            $table->index(["genero_id"], 'datosperso_genero_id_foreign');

            $table->unique(["documento"], 'datosperso_documento_unique');
            $table->timestamps();


            $table->foreign('areas_id', 'datosperso_areas_id_foreign')
                ->references('id')->on('areas')
                ->onDelete('restrict')
                ->onUpdate('restrict');

            $table->foreign('ciudad_id', 'datosperso_ciudad_id_foreign')
                ->references('id')->on('ciudad')
                ->onDelete('restrict')
                ->onUpdate('restrict');

            $table->foreign('estadocivil_id', 'datosperso_estadocivil_id_foreign')
                ->references('id')->on('estadocivil')
                ->onDelete('restrict')
                ->onUpdate('restrict');

            $table->foreign('genero_id', 'datosperso_genero_id_foreign')
                ->references('id')->on('genero')
                ->onDelete('restrict')
                ->onUpdate('restrict');

            $table->foreign('licencia_id', 'datosperso_licencia_id_foreign')
                ->references('id')->on('licencia')
                ->onDelete('restrict')
                ->onUpdate('restrict');

            $table->foreign('tipodocumento_id', 'datosperso_tipodocumento_id_foreign')
                ->references('id')->on('tipodocumento')
                ->onDelete('restrict')
                ->onUpdate('restrict');

            $table->foreign('users_id', 'datosperso_users_id_foreign')
                ->references('id')->on('users')
                ->onDelete('restrict')
                ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
