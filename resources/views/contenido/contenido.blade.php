@extends('principal')

@section('header')
    @include('plantilla.header')
@stop
@section('content')
        @guest
            @include('jobs.contenido')
        @else
            @include('empleados.contenido')
            @include('empresa.final')
        @endguest           
@stop