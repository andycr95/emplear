<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use illuminate\support\facades\storage;
use App\datosp;
use App\ciudad;
use App\estadocivil;
use App\genero;
use App\tipodocumento;
use App\areas;
use App\cursos;
use App\anio;
use App\estado;
use App\nivel;
use App\estudios;
use App\licencia;
use App\vacantes;
use App\categoria;
use App\tipoempleo;
use App\empleo;



class datospController extends Controller
{
    

    public function index()
    {
        
        $datosp = datosp::all();  
        return view('plantilla/ultimos', compact('datosp'));
    }

    public function index2($id)
    {
        
        $datos = datosp::find($id);
        $ciudad = ciudad::all();
        $ciudad2 = ciudad::all();
        $ciudad3 = ciudad::all();
        $ciudad4 = ciudad::all();
        $ciudad5 = ciudad::all();
        $tipoempleo = tipoempleo::all();
        $categoria = categoria::all();
        $estadocivil = estadocivil::all();
        $genero = genero::all();
        $tipodocumento = tipodocumento::all();
        $areas = areas::all();
        $datosp1 = datosp::all();
        $datosp = datosp::all();
        $cursos = cursos::all();
        $anio = anio::all();
        $anio2 = anio::all();
        $anio3 = anio::all();
        $nivel = nivel::all();
        $nivel2 = nivel::all();
        $estado = estado::all();
        $estado2 = estado::all();
        $estudios = estudios::all();
        $licencia = licencia::all();
        $estudios2 = estudios::all();
        return view('empleados/cv', compact('datos','vacantes','datosp1',
        'licencia','ciudad2','estudios2','ciudad3','ciudad4','anio2','anio3',
        'nivel2','estado2','estudios','estado','nivel','cursos','anio','datosp',
        'ciudad', 'estadocivil','tipodocumento','areas','genero','categoria', 'ciudad5', 'tipoempleo'));
        
    }

    

    public function store(Request $request)
    {   
        if (!$request->ajax()) return redirect('/');
        $datosp = new datosp(); 
        $datosp->documento = $request->documento;     
        $datosp->telefono = $request->telefono;     
        $datosp->direccion = $request->direccion;     
        $datosp->licencia = $request->licencia; 
        $datosp->descripcion = $request->descripcion;     
        $datosp->licencia = $request->licencia;
        $datosp->fecha_nacimiento = $request->fecha_nacimiento; 
        $datosp->users_id = $request->users_id; 
        $datosp->areas_id = $request->areas_id; 
        $datosp->tipodocumento_id = $request->tipodocumento_id; 
        $datosp->estadocivil_id = $request->estadocivil_id; 
        $datosp->licencia_id = $request->licencia_id; 
        $datosp->ciudad_id = $request->ciudad_id; 
        $datosp->genero_id = $request->genero_id; 
        $datosp->estado = 'activo';
        $datosp->save();
        return $request->all();

        

    }

    public function store1(Request $request)
    {   
        
        $cursos = new cursos(); 
        $cursos->nombre = $request->nombre;     
        $cursos->institucion = $request->institucion; 
        $cursos->horas = $request->horas; 
        $cursos->users_id = $request->users_id; 
        $cursos->habilidades = $request->habilidades; 
        $cursos->ciudad_id = $request->ciudad_id4; 
        $cursos->anio_id = $request->anio_id3; 
        $cursos->estado = 'activo';
        $cursos->save();

        return back()->with('info', 'Sus datos de formacion complementaria han sido registrados');

    }

    public function store2(Request $request)
    { 

        $estudios = new estudios(); 
        $estudios->institucion = $request->institucion; 
        $estudios->enfasis = $request->enfasis; 
        $estudios->users_id = $request->users_id; 
        $estudios->estado_id = $request->estado_id; 
        $estudios->nivel_id = 1; 
        $estudios->ciudad_id = $request->ciudad_id2; 
        $estudios->anio_id = $request->anio_id; 
        $estudios->estado = $request->estado;
        $estudios->save();
        
        return back()->with('info', 'Sus datos de formacion primaria han sido registrados');

    }
    public function store3(Request $request)
    { 

        $estudios2 = new estudios(); 
        $estudios2->institucion = $request->institucion2; 
        $estudios2->enfasis = $request->enfasis2; 
        $estudios2->users_id = $request->users_id; 
        $estudios2->estado_id = $request->estado_id2; 
        $estudios2->nivel_id = 2; 
        $estudios2->ciudad_id = $request->ciudad_id3; 
        $estudios2->anio_id = $request->anio_id2; 
        $estudios2->estado = $request->estado2;
        $estudios2->save();

        return back()->with('info', 'Sus datos de formacion secundaria han sido registrados');

    }

    public function store4(Request $request)
    { 

        $empleos = new empleo(); 
        $empleos->titulo = $request->titulo; 
        $empleos->nombre = $request->nombre; 
        $empleos->empresa = $request->empresa; 
        $empleos->telefono = $request->telefono; 
        $empleos->ocupacion = $request->ocupacion; 
        $empleos->fecha_inicio = $request->fecha_inicio; 
        $empleos->fecha_fin = $request->fecha_fin; 
        $empleos->users_id = $request->users_id; 
        $empleos->ciudad_id = $request->ciudad_id5; 
        $empleos->categoria_id = $request->categoria_id; 
        $empleos->tipoempleo_id = $request->tipoempleo_id; 
        $empleos->estado = 'activo';
        $empleos->save();

        return back()->with('info', 'Sus datos de empleos han sido registrados, puede agregar mas si desea');

    }

}
