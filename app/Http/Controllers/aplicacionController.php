<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\aplicacion;
use App\vacantes;
use App\ciudad;
use App\areas;
use App\aplicantes;

class aplicacionController extends Controller
{

    public function store(Request $request)
    {
        $aplicacion = new aplicacion();
        $aplicacion->users_id = $request->users_id;
        $aplicacion->vacantes_id = $request->vacantes_id;
        $aplicacion->empresa_id = $request->empresa_id;
        $aplicacion->estado = 'activo';
        $aplicacion->save();
        return back()->with('info', 'Has aplicado para esta oferta, pronto se comunicaran contigo');    

    }


    public function index2($id)
    {

        $publicaciones = vacantes::where('empresa_id',$id)->get();
        return view('empresa/publicaciones', compact('publicaciones'));

    }

}
