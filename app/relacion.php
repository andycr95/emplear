<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class relacion extends Model
{
    protected $fillable = ['nombre', 'estado'];
}
