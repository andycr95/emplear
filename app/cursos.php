<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class cursos extends Model
{
    protected $table = 'cursos';
    protected $fillable = ['institucion','nombre','ciudad_id','horas','habilidades','estado','users_id','anio'];


    public function ciudad()
    {
        return $this->hasone(ciudad::class);
    }

    public function userss()
    {
        return $this->belongsTo(user::class);
    }

    public function anio()
    {
        return $this->hasone(anio::class);
    }

}
