@extends('empleados.principal')

@section('header')
<header class="site-header size-lg text-center" style="background-image: url(/img/curriculum.jpg)">
    <div class="container page-name">
        <h1 class="text-center">Registre sus datos personales</h1>
    </div>
</header>
@stop

@section('content')
    <section>   
            @if (session()->has('info'))
            <div class="alert alert-success">
                {{ session('info') }}
            </div>
            @endif
        <div class="panel-group" id="accordion">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse1">Datos Personales</a>
                    </h4>
                </div>
                <div id="collapse1" class="panel-collapse collapse">
                    <div class="panel-body">
                            <!--Formulario Datos Rersonales-->
                        <div class="container">
                            <form class="form-horizontal" role="form" method="POST" action="/hojav">
                                    @csrf
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group {{ $errors->has('fecha_nacimiento') ? ' is-invalid' : '' }}">
                                                <label for="fecha_nacimiento" class="col-md-4 control-label">Fecha Nacimiento</label>
                                                <div class="col-md-6">
                                                    <input id="fecha_nacimiento" type="date" class="form-control" name="fecha_nacimiento" value="" required autofocus>
                                                    @if ($errors->has('fecha_nacimiento'))
                                                        <span class="invalid-feedback">
                                                            <strong>{{ $errors->first('fecha_nacimiento') }}</strong>
                                                        </span>
                                                    @endif
                                                </div>
                                            </div>
                                        <div class="form-group {{ $errors->has('telefono') ? ' is-invalid' : '' }}">
                                                <label for="telefono" class="col-md-4 control-label">Telefono</label>
                                                <div class="col-md-6">
                                                    <input id="telefono" type="text" class="form-control" name="telefono" value="" required autofocus>
                                                    @if ($errors->has('telefono'))
                                                        <span class="invalid-feedback">
                                                            <strong>{{ $errors->first('telefono') }}</strong>
                                                        </span>
                                                    @endif
                                                </div>
                                            </div>
                                        <div class="form-group {{ $errors->has('direccion') ? ' is-invalid' : '' }}">
                                                <label for="direccion" class="col-md-4 control-label">direccion</label>
                                                <div class="col-md-6">
                                                    <input id="direccion" type="text" class="form-control" name="direccion" value="" required autofocus>
                                                        @if ($errors->has('direccion'))
                                                            <span class="invalid-feedback">
                                                                <strong>{{ $errors->first('direccion') }}</strong>
                                                            </span>
                                                        @endif
                                                    </div>
                                                </div>
                                        <div class="form-group {{ $errors->has('genero_id') ? 'is-invalid' : '' }}">
                                                    <label for="genero_id" class="col-md-4 control-label" name="genero_id">Genero</label>
                                                        <div class="col-md-6">
                                                            <select name="genero_id" class="form-control">
                                                        <option class="form-inputs" name="" value="#">Seleccione una opcion</option>
                                                        @foreach($genero as $genero)
                                                            <option  id="genero_id" class="form-inputs" name="genero_id" value="{{ $genero->id }}" >{{$genero->nombre}}</option>
                                                        @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                        <div class="form-group {{ $errors->has('ciudad_id') ? 'is-invalid' : '' }}">
                                                    <label for="ciudad_id" class="col-md-4 control-label" name="ciudad_id">Ciudad</label>
                                                        <div class="col-md-6">
                                                    <select name="ciudad_id" class="form-control">
                                                            <option class="form-inputs" name="" value="#">Seleccione una opcion</option>
                                                            @foreach($ciudad as $ciudad)
                                                                <option  id="ciudad_id" class="form-inputs" name="ciudad_id" value="{{ $ciudad->id }}" >{{$ciudad->nombre}}</option>
                                                            @endforeach
                                                    </select>
                                                        </div>
                                                    </div>
                                        <div class="form-group{{ $errors->has('estadocivil_id') ? 'is-invalid' : '' }}">
                                                <label for="estadocivi_idl" class="col-md-4 control-label" name="estadocivil_id">Estado Civil</label>
                                                    <div class="col-md-6">
                                                            <select name="estadocivil_id" class="form-control">
                                                        <option class="form-inputs" name="" value="#">Seleccione una opcion</option>
                                                        @foreach($estadocivil as $estadocivil)
                                                            <option  id="estadocivil_id" class="form-inputs" name="estadocivil_id" value="{{ $estadocivil->id }}" >{{$estadocivil->nombre}}</option>
                                                        @endforeach
                                                            </select>
                                                    </div>
                                                </div>
                                        <div class="form-group{{ $errors->has('licencia_id') ? 'is-invalid' : '' }}">
                                                <label for="licencia_id" class="col-md-4 control-label" name="licencia_id">Licencia</label>
                                                    <div class="col-md-6">
                                                        <select name="licencia_id" class="form-control">
                                                            <option class="form-inputs" name="" value="#">Seleccione una opcion</option>
                                                            @foreach($licencia as $licencia)
                                                            <option  id="licencia_id" class="form-inputs" name="licencia_id" value="{{ $licencia->id }}" >{{$licencia->nombre}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                        <div class="form-group {{ $errors->has('licencia') ? ' is-invalid' : '' }}">
                                                <label for="licencia" class="col-md-4 control-label">Numero de Licencia</label>
                                                    <div class="col-md-6">
                                                        <input id="licencia" type="text" class="form-control" name="licencia" value="" required autofocus>
                                                            @if ($errors->has('licencia'))
                                                                <span class="invalid-feedback">
                                                                    <strong>{{ $errors->first('licencia') }}</strong>
                                                                </span>
                                                            @endif
                                                        </div>
                                                </div>
                                        <div class="form-group {{ $errors->has('areas_id') ? 'is-invalid' : '' }}">
                                                <label for="areas_id" class="col-md-4 control-label" name="areas_id">Profesion</label>
                                                    <div class="col-md-6">
                                                        <select name="areas_id" class="form-control">
                                                    <option class="form-inputs" name="" value="#">Seleccione una opcion</option>  
                                                    @foreach($areas as $areas)
                                                        <option  id="areas_id" class="form-inputs" name="areas_id" value="{{ $areas->id }}" >{{$areas->nombre}}</option>
                                                    @endforeach
                                                        </select>
                                                    </div>
                                                </div> 
                                        <div class="form-group {{ $errors->has('areas_id') ? 'is-invalid' : '' }}">
                                            <label for="areas_id" class="col-md-4 control-label" name="areas_id">Profesion</label>
                                                <div class="col-md-6">
                                                <input type="file" id="file" name="file"></input>
                                                </div>
                                            </div>           
                                        <div class="form-group{{ $errors->has('tipodocumento_id') ? 'is-invalid' : '' }}">
                                                <label for="tipodocumento_id" class="col-md-4 control-label" name="tipodocumento_id">Tipo Documento</label>
                                                            <div class="col-md-6">
                                                        <select name="tipodocumento_id" class="form-control">
                                                                <option class="form-inputs" name="" value="#">Seleccione una opcion</option> 
                                                                @foreach($tipodocumento as $tipodocumento)
                                                                    <option  id="tipodocumento_id" class="form-inputs" name="tipodocumento_id" value="{{ $tipodocumento->id }}" >{{$tipodocumento->nombre}}</option>
                                                                @endforeach
                                                        </select>
                                                            </div>
                                                        </div> 
                                        <div class="form-group {{ $errors->has('documento') ? ' is-invalid' : '' }}">
                                            <label for="documento" class="col-md-4 control-label">Documento</label>
                                                <div class="col-md-6">
                                                    <input id="documento" type="text" class="form-control" name="documento" value="" required autofocus>
                                                    @if ($errors->has('documento'))
                                                        <span class="invalid-feedback">
                                                            <strong>{{ $errors->first('documento') }}</strong>
                                                        </span>
                                                    @endif
                                                </div>
                                            </div> 
                                    </div>
                                </div>
                                <hr />
                                <div class="row">
                                    <div class="col-md-12">
                                        <div id="field_descripcion" class="form-group">
                                            <label for="descripcion" class="control-label">Hablanos mas de ti.</label>    
                                                <div class="controls">
                                                    <textarea id="descripcion" class="form-control" name="descripcion" cols="50" rows="10"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <div class="col-md-12 text-center">
                                    <button type="submit" id="users_id" name="users_id" class="btn btn-primary" value="{{auth::user()->id}}" >Guardar</button>
                                    </div>
                            </form>
                        </div>   
                    </div>
            </div>
            <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse2">Formacion Academica</a>
                        </h4>
                    </div>
                    <div id="collapse2" class="panel-collapse collapse">
                        <div class="panel-body"> 
                            <div class="panel panel-secundary">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse5">Primaria</a>
                                    </h4>
                                </div>
                                <div id="collapse5" class="panel-collapse collapse">
                                    <div class="panel-body">
                                    <!--Formulario Datos Rersonales-->
                                        <div class="container">
                                            <form class="form-horizontal" role="form" method="POST" action="/hojavp">
                                                @csrf
                                                <div class="row">
                                                    <div class="col-md-12">
                                                            <div class="form-group {{ $errors->has('institucion') ? ' is-invalid' : '' }}">
                                                                <label for="institucion" class="col-md-4 control-label">Nombre Institucion</label>
                                                                    <div class="col-md-6">
                                                                        <input id="institucion" type="text" class="form-control" name="institucion" value="" required autofocus>
                                                                            @if ($errors->has('institucion'))
                                                                                <span class="invalid-feedback">
                                                                                    <strong>{{ $errors->first('institucion') }}</strong>
                                                                                </span>
                                                                            @endif
                                                                        </div>
                                                                </div>
                                                            <div class="form-group {{ $errors->has('enfasis') ? ' is-invalid' : '' }}">
                                                                <label for="enfasis" class="col-md-4 control-label">enfasis</label>
                                                                <div class="col-md-6">
                                                                    <input id="enfasis" type="text" class="form-control" name="enfasis" value="" required autofocus>
                                                                        @if ($errors->has('enfasis'))
                                                                            <span class="invalid-feedback">
                                                                                <strong>{{ $errors->first('enfasis') }}</strong>
                                                                            </span>
                                                                        @endif
                                                                    </div>
                                                                </div>
                                                            <div class="form-group {{ $errors->has('ciudad_id2') ? 'is-invalid' : '' }}">
                                                                <label for="ciudad_id2" class="col-md-4 control-label" name="ciudad_id2">Ciudad</label>
                                                                    <div class="col-md-6">
                                                                        <select name="ciudad_id2" class="form-control">
                                                                            <option class="form-control" name="" value="#">Seleccione una opcion</option>
                                                                                @foreach($ciudad2 as $ciudad2)
                                                                            <option  id="ciudad_id2" class="form-control" name="ciudad_id2" value="{{ $ciudad2->id }}" >{{$ciudad2->nombre}}</option>
                                                                                @endforeach
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            <div class="form-group{{ $errors->has('estado_id') ? 'is-invalid' : '' }}">
                                                                <label for="estado_id" class="col-md-4 control-label" name="estado_id">Estado</label>
                                                                    <div class="col-md-6">
                                                                        <select name="estado_id" class="form-control">
                                                                            <option class="form-inputs" name="" value="#">Seleccione una opcion</option>
                                                                        @foreach($estado as $estado)
                                                                            <option  id="estado_id" class="form-control" name="estado_id" value="{{ $estado->id }}" >{{$estado->nombre}}</option>
                                                                        @endforeach
                                                                        </select>
                                                                    </div>
                                                                </div>  
                                                            <div class="form-group {{ $errors->has('anio_id') ? 'is-invalid' : '' }}">
                                                                <label for="anio_id" class="col-md-4 control-label" name="anio_id">Año</label>
                                                                    <div class="col-md-6">
                                                                        <select name="anio_id" class="form-control">
                                                                            <option class="form-inputs" name="" value="#">Seleccione una opcion</option>  
                                                                        @foreach($anio3 as $anio3)
                                                                            <option  id="anio_id" class="form-control" name="anio_id" value="{{ $anio3->id }}" >{{$anio3->nombre}}</option>
                                                                        @endforeach
                                                                        </select>
                                                                    </div>
                                                                </div>        
                                                        </div>   
                                                    </div>
                                                </div>
                                                <div class="col-md-12 text-center">
                                                    <button type="submit" id="users_id" name="users_id" class="btn btn-primary" value="{{auth::user()->id}}" >Guardar</button>
                                                    <button type="submit" class="btn btn-success">Actualizar</button>
                                                </div>
                                            </form>  
                                    </div>
                                </div>
                                </div>    
                            <div class="panel panel-secundary">
                                    <div class="panel-heading">
                                        <h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion" href="#collapse4">Secundaria</a></h4>
                                        </div>
                                            <div id="collapse4" class="panel-collapse collapse">
                                                <div class="panel-body">
                                                <!--Formulario Datos Rersonales-->
                                                    <div class="container">
                                                        <form class="form-horizontal" role="form" method="POST" action="/hojavs">
                                                            @csrf
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                       <div class="form-group {{ $errors->has('institucion2') ? ' is-invalid' : '' }}">
                                                                            <label for="institucion2" class="col-md-4 control-label">Nombre Institucion</label>
                                                                                <div class="col-md-6">
                                                                                    <input id="institucion2" type="text" class="form-control" name="institucion2" value="" required autofocus>
                                                                                        @if ($errors->has('institucion2'))
                                                                                            <span class="invalid-feedback">
                                                                                                <strong>{{ $errors->first('institucion2') }}</strong>
                                                                                            </span>
                                                                                        @endif
                                                                                    </div>
                                                                            </div>
                                                                        <div class="form-group {{ $errors->has('enfasis2') ? ' is-invalid' : '' }}">
                                                                            <label for="enfasis2" class="col-md-4 control-label">enfasis</label>
                                                                            <div class="col-md-6">
                                                                                <input id="enfasis2" type="text" class="form-control" name="enfasis2" value="" required autofocus>
                                                                                    @if ($errors->has('enfasis2'))
                                                                                        <span class="invalid-feedback">
                                                                                            <strong>{{ $errors->first('enfasis2') }}</strong>
                                                                                        </span>
                                                                                    @endif
                                                                                </div>
                                                                            </div>
                                                                        <div class="form-group {{ $errors->has('ciudad_id3') ? 'is-invalid' : '' }}">
                                                                            <label for="ciudad_id3" class="col-md-4 control-label" name="ciudad_id3">Ciudad</label>
                                                                                <div class="col-md-6">
                                                                                    <select name="ciudad_id3" class="form-control">
                                                                                        <option class="form-inputs" name="" value="#">Seleccione una opcion</option>
                                                                                            @foreach($ciudad3 as $ciudad3)
                                                                                        <option  id="ciudad_id3" class="form-inputs" name="ciudad_id3" value="{{ $ciudad3->id }}" >{{$ciudad3->nombre}}</option>
                                                                                            @endforeach
                                                                                    </select>
                                                                                </div>
                                                                            </div>
                                                                        <div class="form-group{{ $errors->has('estado_id2') ? 'is-invalid' : '' }}">
                                                                            <label for="estado_id2" class="col-md-4 control-label" name="estado_id2">Estado Civil</label>
                                                                                <div class="col-md-6">
                                                                                    <select name="estado_id2" class="form-control">
                                                                                        <option class="form-inputs" name="" value="#">Seleccione una opcion</option>
                                                                                    @foreach($estado2 as $estado2)
                                                                                        <option  id="estado_id2" class="form-inputs" name="estado_id2" value="{{ $estado2->id }}" >{{$estado2->nombre}}</option>
                                                                                    @endforeach
                                                                                    </select>
                                                                                </div>
                                                                            </div>  
                                                                        <div class="form-group {{ $errors->has('anio_id2') ? 'is-invalid' : '' }}">
                                                                            <label for="anio_id2" class="col-md-4 control-label" name="anio_id2">Año</label>
                                                                                <div class="col-md-6">
                                                                                    <select name="anio_id2" class="form-control">
                                                                                        <option class="form-inputs" name="" value="#">Seleccione una opcion</option>  
                                                                                    @foreach($anio2 as $anio2)
                                                                                        <option  id="anio_id2" class="form-inputs" name="anio_id2" value="{{ $anio2->id }}" >{{$anio2->nombre}}</option>
                                                                                    @endforeach
                                                                                    </select>
                                                                                </div>
                                                                            </div>        
                                                                
                                                                    
                                                                </div>   
                                                            </div>
                                                            <div class="col-md-12 text-center">
                                                                    <button type="submit" id="users_id" name="users_id" class="btn btn-primary" value="{{auth::user()->id}}" >Guardar</button>
                                                                    <button type="submit" class="btn btn-success">Actualizar</button>
                                                                </div>
                                                        </form>    
                                                    </div>
                                                </div>
                                            </div>        
                            <div class="panel panel-secundary">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse3">Cursos</a>
                                    </h4>
                                </div>
                                <div id="collapse3" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <div class="container">
                                            <!--Formacion-->
                                                <form class="form-horizontal" role="form" method="POST" action="/hojavc">
                                                    @csrf
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div id="masterQuestion">
                                                                <div id="type1" class="questionSet">
                                                                    <div class="col-md-6">
                                                                        <h1>Cursos</h1>
                                                                        <input type="hidden" id="remember_token" name="remember_token" value="">
                                                                        <div class="form-group {{ $errors->has('nombre') ? ' is-invalid' : '' }}">
                                                                            <label for="nombre" class="col-md-4 control-label">Nombre</label>
                                                                            <div class="col-md-6">
                                                                                <input id="nombre" type="text" class="form-control" name="nombre" value="" required autofocus>
                                                                                    @if ($errors->has('nombre'))
                                                                                    <span class="invalid-feedback">
                                                                                        <strong>{{ $errors->first('nombre') }}</strong>
                                                                                    </span>
                                                                                    @endif
                                                                                </div>
                                                                            </div>
                                                                        <div class="form-group {{ $errors->has('Institucion3') ? ' is-invalid' : '' }}">
                                                                            <label for="Institucion3" class="col-md-4 control-label">Institucion</label>
                                                                            <div class="col-md-6">
                                                                                <input id="Institucion3" type="text" class="form-control" name="Institucion3" value="" required autofocus>
                                                                                    @if ($errors->has('Institucion'))
                                                                                    <span class="invalid-feedback">
                                                                                        <strong>{{ $errors->first('Institucion') }}</strong>
                                                                                    </span>
                                                                                    @endif
                                                                                </div>
                                                                            </div>
                                                                        <div class="form-group {{ $errors->has('horas') ? ' is-invalid' : '' }}">
                                                                            <label for="horas" class="col-md-4 control-label">Duracion</label>
                                                                            <div class="col-md-6">
                                                                                <input id="horas" type="text" class="form-control" name="horas" value="" placeholder="Horas" required autofocus>
                                                                                    @if ($errors->has('horas'))
                                                                                    <span class="invalid-feedback">
                                                                                        <strong>{{ $errors->first('horas') }}</strong>
                                                                                    </span>
                                                                                    @endif
                                                                                </div>
                                                                            </div>
                                                                        <div class="form-group {{ $errors->has('habilidades') ? ' is-invalid' : '' }}">
                                                                            <label for="habilidades" class="col-md-4 control-label">Habilidades</label>
                                                                                <div class="col-md-6">
                                                                                    <input id="habilidades" type="text" class="form-control" name="habilidades" value="" required autofocus>
                                                                                        @if ($errors->has('habilidades'))
                                                                                            <span class="invalid-feedback">
                                                                                                <strong>{{ $errors->first('habilidades') }}</strong>
                                                                                            </span>
                                                                                        @endif
                                                                                </div>
                                                                            </div>
                                                                        <div class="form-group{{ $errors->has('ciudad_id4') ? 'is-invalid' : '' }}">
                                                                            <label for="ciudad_id4" class="col-md-4 control-label" name="ciudad_id4">Ciudad</label>
                                                                                <div class="col-md-6">
                                                                                    <select name="ciudad_id4" class="form-control">
                                                                                        <option class="form-inputs" name="" value="#">Seleccione una opcion</option>
                                                                                        @foreach($ciudad4 as $ciudad4)
                                                                                        <option  id="ciudad_id4" class="form-inputs" name="ciudad_id4" value="{{ $ciudad4->id }}" >{{$ciudad4->nombre}}</option>
                                                                                            @endforeach
                                                                                    </select>
                                                                                </div>
                                                                            </div>  
                                                                        <div class="form-group {{ $errors->has('anio_id3') ? 'is-invalid' : '' }}">
                                                                            <label for="anio_id3" class="col-md-4 control-label" name="anio_id3">Año</label>
                                                                                <div class="col-md-6">
                                                                                    <select name="anio_id3" class="form-control">
                                                                                        <option class="form-inputs" name="" value="#">Seleccione una opcion</option>  
                                                                                            @foreach($anio as $anio)
                                                                                                <option  id="anio_id3" class="form-inputs" name="anio_id3" value="{{ $anio->id }}" >{{$anio->nombre}}</option>
                                                                                            @endforeach
                                                                                    </select>
                                                                                </div>
                                                                            </div>        
                                                                    </div>
                                                                    <div class="row">
                                                                        <div id="questions">
                                                                            </div>
                                                                            <p><button class="btn btn-primary" id="addQuestion">Añadir curso</button></p>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12 text-center">
                                                            <button type="submit" id="users_id" name="users_id" class="btn btn-primary" value="{{auth::user()->id}}" >Guardar</button>
                                                            <button type="submit" class="btn btn-success">Actualizar</button>
                                                        </div>
                                                </form>
                                        </div>
                                </div> 
                            </div>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse3">Formación Laboral</a>
                    </h4>
                </div>
                <div id="collapse3" class="panel-collapse collapse">
                    <div class="panel-body">
                            <!--Formulario Formacioón Laboral-->
                        <div class="container">
                            <form class="form-horizontal" role="form" method="POST" action="/hojave">
                                    @csrf
                                <div class="row">
                                    <div class="col-md-12">
                                         <div class="form-group {{ $errors->has('titulo') ? ' is-invalid' : '' }}">
                                                <label for="titulo" class="col-md-4 control-label">Titulo</label>
                                                    <div class="col-md-6">
                                                        <input id="titulo" type="text" class="form-control" name="titulo" value="" required autofocus>
                                                            @if ($errors->has('titulo'))
                                                                <span class="invalid-feedback">
                                                                    <strong>{{ $errors->first('titulo') }}</strong>
                                                                </span>
                                                            @endif
                                                    </div>
                                                </div>
                                        <div class="form-group {{ $errors->has('nombre') ? ' is-invalid' : '' }}">
                                            <label for="nombre" class="col-md-4 control-label">Nombre</label>
                                                <div class="col-md-6">
                                                    <input id="nombre" type="text" class="form-control" name="nombre" value="" required autofocus>
                                                        @if ($errors->has('nombre'))
                                                            <span class="invalid-feedback">
                                                                <strong>{{ $errors->first('nombre') }}</strong>
                                                            </span>
                                                        @endif
                                                </div>
                                            </div>
                                        <div class="form-group {{ $errors->has('empresa') ? ' is-invalid' : '' }}">
                                            <label for="empresa" class="col-md-4 control-label">Empresa</label>
                                                <div class="col-md-6">
                                                    <input id="empresa" type="text" class="form-control" name="empresa" value="" required autofocus>
                                                        @if ($errors->has('empresa'))
                                                            <span class="invalid-feedback">
                                                                <strong>{{ $errors->first('empresa') }}</strong>
                                                            </span>
                                                        @endif
                                                </div>
                                            </div>
                                        <div class="form-group {{ $errors->has('telefono') ? ' is-invalid' : '' }}">
                                            <label for="telefono" class="col-md-4 control-label">Telefono</label>
                                                <div class="col-md-6">
                                                    <input id="telefono" type="text" class="form-control" name="telefono" value="" required autofocus>
                                                        @if ($errors->has('telefono'))
                                                            <span class="invalid-feedback">
                                                                <strong>{{ $errors->first('telefono') }}</strong>
                                                            </span>
                                                        @endif
                                                </div>
                                            </div>
                                        <div class="form-group {{ $errors->has('ocupacion') ? ' is-invalid' : '' }}">
                                            <label for="ocupacion" class="col-md-4 control-label">Ocupacion</label>
                                                <div class="col-md-6">
                                                    <input id="ocupacion" type="text" class="form-control" name="ocupacion" value="" required autofocus>
                                                        @if ($errors->has('ocupacion'))
                                                            <span class="invalid-feedback">
                                                                <strong>{{ $errors->first('ocupacion') }}</strong>
                                                            </span>
                                                        @endif
                                                </div>
                                            </div> 
                                        <div class="form-group {{ $errors->has('fecha_inicio') ? ' is-invalid' : '' }}">
                                            <label for="fecha_inicio" class="col-md-4 control-label">Fecha inicio</label>
                                                <div class="col-md-6">
                                                    <input id="fecha_inicio" type="date" class="form-control" name="fecha_inicio" value="" required autofocus>
                                                        @if ($errors->has('fecha_inicio'))
                                                            <span class="invalid-feedback">
                                                                <strong>{{ $errors->first('fecha_inicio') }}</strong>
                                                            </span>
                                                        @endif
                                                </div>
                                            </div>
                                        <div class="form-group {{ $errors->has('fecha_fin') ? ' is-invalid' : '' }}">
                                            <label for="fecha_fin" class="col-md-4 control-label">Fecha fin</label>
                                                <div class="col-md-6">
                                                    <input id="fecha_fin" type="date" class="form-control" name="fecha_fin" value="" required autofocus>
                                                        @if ($errors->has('fecha_fin'))
                                                            <span class="invalid-feedback">
                                                                <strong>{{ $errors->first('fecha_fin') }}</strong>
                                                            </span>
                                                        @endif
                                                </div>
                                            </div>     
                                        <div class="form-group{{ $errors->has('ciudad_id5') ? 'is-invalid' : '' }}">
                                            <label for="ciudad_id5" class="col-md-4 control-label" name="ciudad_id5">Ciudad</label>
                                                <div class="col-md-6">
                                                    <select name="ciudad_id5" class="form-control">
                                                        <option class="form-inputs" name="" value="#">Seleccione una opcion</option>
                                                        @foreach($ciudad5 as $ciudad5)
                                                        <option  id="ciudad_id5" class="form-inputs" name="ciudad_id5" value="{{ $ciudad5->id }}" >{{$ciudad5->nombre}}</option>
                                                            @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                        <div class="form-group{{ $errors->has('tipoempleo_id') ? 'is-invalid' : '' }}">
                                            <label for="tipoempleo_id" class="col-md-4 control-label" name="tipoempleo_id">Tipo de empleo</label>
                                                <div class="col-md-6">
                                                    <select name="tipoempleo_id" class="form-control">
                                                        <option class="form-inputs" name="" value="#">Seleccione una opcion</option>
                                                        @foreach($tipoempleo as $tipoempleo)
                                                        <option  id="tipoempleo_id" class="form-inputs" name="tipoempleo_id" value="{{ $tipoempleo->id }}" >{{$tipoempleo->nombre}}</option>
                                                            @endforeach
                                                    </select>
                                                </div>
                                            </div>     
                                        <div class="form-group {{ $errors->has('categoria_id') ? 'is-invalid' : '' }}">
                                            <label for="categoria_id" class="col-md-4 control-label" name="categoria_id">Categoria</label>
                                                <div class="col-md-6">
                                                    <select name="categoria_id" class="form-control">
                                                        <option class="form-inputs" name="" value="#">Seleccione una opcion</option>  
                                                            @foreach($categoria as $categoria)
                                                                <option  id="categoria_id" class="form-inputs" name="categoria_id" value="{{ $categoria->id }}" >{{$categoria->nombre}}</option>
                                                            @endforeach
                                                    </select>
                                                </div>
                                            </div> 
              
                                    </div>
                                </div>
                                <hr />
                                <div class="col-md-12 text-center">
                                    <button type="submit" id="users_id" name="users_id" class="btn btn-primary" value="{{auth::user()->id}}" >Guardar</button>
                                    </div>
                            </form>
                        </div>   
                    </div>
            </div>    
        </div> 
        <hr />    
    </section>
@stop
           