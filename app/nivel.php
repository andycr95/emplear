<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class nivel extends Model
{
    protected $table = 'nivel';
    protected $fillable = ['nombre', 'estado'];

    public function estudios()
    {
        return $this->hasMany(estudios::class);
    }
}
