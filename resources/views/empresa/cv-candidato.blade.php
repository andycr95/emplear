@extends('empresa.principal')

@if($datos->datosp != '')
    @section('header')
        <header class="site-header size-lg text-center" style="background-image: url(/img/curriculum.jpg)">
            
        </header>
    @endsection
    @section('content')
        <section>
            <div class="container">
                <div class="row">
                    <h2  class="control-label">Datos Personales</h2>
                    <div class="col-md-9">                  
                        <div class="form-group">
                            <label for="telefono" class="col-md-4 control-label">Nombre completo</label>
                            {{$datos->name}} {{$datos->apellidos}}
                            </div>
                        <div class="form-group">
                            @if($datos->datosp->fecha_nacimiento != '')
                                <label for="contacto" class="col-md-4 control-label">Fecha de nacimiento</label>
                                    {{$datos->datosp->fecha_nacimiento}} 
                                </div>
                            @else
                            @endif                        
                        <div class="form-group">
                            <label for="contacto" class="col-md-4 control-label">Profesion</label>
                                {{$datos->datosp->areas->nombre}} 
                            </div>        
                        <div class="form-group">
                            <label for="facebook" class="col-md-4 control-label">Estado civil</label>
                            {{$datos->datosp->estadocivil->nombre}}
                            </div>
                        <div class="form-group">
                            <label for="ciudad" class="col-md-4 control-label">Ciudad</label>
                            {{$datos->datosp->ciudad->nombre}} -  {{$datos->datosp->ciudad->departamento->nombre}}                                
                            </div>
                                
                        <div class="form-group">
                            <label for="nit" class="col-md-4 control-label">Genero</label>
                            {{$datos->datosp->genero->nombre}}
                            </div>                  
                    </div>
                    <div class="col-md-3">
                        @if ( $datos->datosp->foto != '' )
                        <card>
                            <img class="img-xs" src="{{ $datos->datosp->foto}}" alt="">    
                        </card>
                        @else
                            <img src="/img/avatar.jpg" alt="">  
                        @endif
                    </div>
                </div>    
                <div class="row">
                    <div class="col-md-12">
                        <div id="field_descripcion" class="form-group">
                            @if($datos->datosp->descripcion != '')
                                <h2 for="descripcion" class="control-label">Descripcion</h2>  
                                {{$datos->datosp->descripcion}} 
                            </div>
                            @else
                            @endif  
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <h2 for="descripcion" class="control-label">Formacion academica</h2> 
                        @if (auth::user()->estudios != '')
                            <div class="panel-group" id="accordion">
                                @foreach ($dato as $dato)
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <a data-toggle="collapse" data-parent="#accordion" href="#collapse{{ $dato->id }}">{{ $dato->nivel->nombre }}</a>
                                            </h4>
                                        </div>
                                        <div id="collapse{{ $dato->id }}" class="panel-collapse collapse">
                                            <div class="panel-body">
                                                <div class="container">
                                                    <div class="form-group">
                                                        @if($dato->institucion != '')
                                                        <label for="intitucion" class="col-md-4 control-label">Nombre Intitucion</label>
                                                        {{$dato->institucion}} 
                                                    </div>
                                                    @else
                                                    @endif   
                                                    <div class="form-group">
                                                        @if($dato->enfasis != '')
                                                        <label for="intitucion" class="col-md-4 control-label">enfasis</label>
                                                        {{$dato->enfasis}} 
                                                    </div>
                                                    @else
                                                    @endif 
                                                    <div class="form-group">
                                                        <label for="intitucion" class="col-md-4 control-label">Ciudad</label>  
                                                        {{$dato->ciudad->nombre}} 
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="intitucion" class="col-md-4 control-label">Estado</label>  
                                                        {{$dato->estado->nombre}} 
                                                    </div> 
                                                    <div class="form-group">
                                                        <label for="intitucion" class="col-md-4 control-label">Año</label>  
                                                        {{$dato->anio->nombre}} 
                                                    </div>      
                                                </div>
                                            </div> 
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        @else
                        @endif
                    </div>
                    <div class="col-md-6">
                        @if (auth::user()->empleos != '')
                            <h2 for="descripcion" class="control-label">Experiencia laboral</h2> 
                            <div class="panel-group" id="accordion">
                                @foreach ($emple as $emple)
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <a data-toggle="collapse" data-parent="#accordion" href="#collapse{{ $emple->id }}">{{ $emple->titulo }}</a>
                                            </h4>
                                        </div>
                                        <div id="collapse{{ $emple->id }}" class="panel-collapse collapse">
                                            <div class="panel-body">
                                                <div class="container"> 
                                                    <div class="form-group">
                                                        @if($emple->nombre != '')
                                                            <label for="nombre" class="col-md-4 control-label">Nombre</label>
                                                                {{$emple->nombre}} 
                                                        </div>
                                                        @else
                                                        @endif 
                                                    <div class="form-group">
                                                        @if($emple->empresa != '')
                                                            <label for="empresa" class="col-md-4 control-label">Empresa</label>
                                                                {{$emple->empresa}} 
                                                        </div>
                                                        @else
                                                        @endif 
                                                    <div class="form-group">
                                                        @if($emple->telefono != '')
                                                            <label for="telefono" class="col-md-4 control-label">Telefono</label>
                                                            {{$emple->telefono}} 
                                                        </div>
                                                        @else
                                                        @endif 
                                                    <div class="form-group">
                                                        @if($emple->fecha_inicio != '')
                                                            <label for="fecha_inicio" class="col-md-4 control-label">Fecha inicio</label>
                                                            {{$emple->fecha_inicio}} 
                                                        </div>
                                                        @else
                                                        @endif 
                                                    <div class="form-group">
                                                        @if($emple->fecha_fin != '')
                                                            <label for="fecha_fin" class="col-md-4 control-label">Fecha fin</label>
                                                            {{$emple->fecha_fin}} 
                                                        </div>
                                                        @else
                                                        @endif 
                                                    <div class="form-group">
                                                            <label for="categoria" class="col-md-4 control-label">categoria</label>  
                                                            {{$emple->categoria->nombre}} 
                                                        </div>
                                                    <div class="form-group">
                                                            <label for="intitucion" class="col-md-4 control-label">Ciudad</label>  
                                                            {{$emple->ciudad->nombre}} 
                                                        </div>
                                                    <div class="form-group">
                                                        <label for="intitucion" class="col-md-4 control-label">Tipo de contrato</label>  
                                                        {{$emple->tipoempleo->nombre}} 
                                                        </div>      
                                                </div>  
                                            </div>
                                        </div> 
                                    </div>
                                @endforeach
                            </div>
                        @else
                        @endif
                    </div>
                </div>
            </div>
        </section>
    @endsection   
@else
    @section('header')
        <header class="site-header size-lg text-center" style="background-image: url(/img/curriculum.jpg)">
            <div class="container page-name text-center">
                <h1 class="text-center">Hola</h1>
                <img src="/img/warning.png">
                <h2 class="text-center">Este candidato no ha llenado sus datos personales</h2>
            </div>
        </header>
    @endsection
@endif