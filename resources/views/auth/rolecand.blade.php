@extends('principal')

@section('header')
<div class="container page-name">
        <h1 class="text-center">Bienvenido a <b>{{ config('app.name') }}</b></h1>
        <p class="lead text-center">Encuentra el trabajo que deseas!</p>
    </div>

<div class="container page-name">
    <form class="form-horizontal" role="form" method="POST" action="/Bienvenido">
        @csrf
            <div class="col-md-12 text-center">
                <input type="hidden" name="role_id" value="1">
                <button type="summit" id="users_id" name="users_id" class="btn btn-primary" value="{{auth::user()->id}}" >Continuar</button>
            </div>
    </form>
</div>   
@stop