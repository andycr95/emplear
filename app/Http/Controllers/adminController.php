<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\users;
use App\role;
use App\assignedRole;
use App\vacantes;
use App\empresa;
use App\datosp;
use App\aplicacion;
use App\aplicantes;
use Carbon\Carbon;


class adminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(request $request)
    {
        $current = Carbon::now();
        $current = new Carbon();
        $yesterday = Carbon::yesterday();
        $lastvacant = vacantes::where('created_at', '>', $yesterday);
        $users = datosp::all();
        $empresas = empresa::all();
        return view('admin/dashboard', compact('lastvacant', 'users', 'empresas'));
    }

    public function usuariosindex(request $request)
    {
        $buscar = $request->search;   

        if ($buscar != '') {
            $users = users::search($buscar)->get();
        }else {
            $users = users::with('roles')->get();
            
        }       

        return view('admin/boxusers', compact('users','lastvacant'));
    }

    public function vacante($id)
    {
        $vacante = aplicacion::where('vacantes_id',$id)->get();
        
        return view('admin.vacantes-empresa', compact('vacante'));
        
    }

    public function empresaindex(request $request)
    {
        $buscar = $request->search;
        $yesterday = Carbon::yesterday();

        $lastvacant = vacantes::where('created_at', '>', $yesterday);
        

        if ($buscar != '') {
            $empresa = empresa::search($buscar)->get();
        }else {
            $empresa = empresa::with('vacantes')->get();
            
        }

        

        return view('admin/empresas', compact('empresa','lastvacant'));
    }
    

    public function rolestore(request $request)
    {
        $assignedRole = assignedRole::create($request->all());
        return redirect()->back()->with('info','Role asignado');       
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function aplicantstore(Request $request)
    {
        
        $aplicantes = new aplicantes();
        $aplicantes->users_id = $request->users_id;
        $aplicantes->empresa_id = $request->empresa_id;
        $aplicantes->aplicacion_id = $request->aplicacion_id;
        $aplicantes->estado = $request->estado;
        $aplicantes->save();        
        return redirect()->back()->with('info','Candidato aprobado');      
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function usershow($id)
    {
        
        $estado = [
            'activo',
            'inactivo'
        ];
        $datos = users::with('roles')->findOrFail($id);
        $role = role::all();
        return view('admin/edit', compact('datos','role','estado'));
    
    }

    public function empreshow($id)
    {
        $datos = empresa::findOrFail($id);
        $v = vacantes::where('empresa_id',$id)->get();
        return view('admin/empresaedit', compact('datos', 'v'));
    
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function editestado(Request $request, $id)
    {
        $users = users::findOrFail($request->id);
        $users->estado = $request->estado;
        $users->save();
        
        return redirect()->back()->with('info', 'Estado actualizado'); 
    }

    public function empresaesta(Request $request, $id)
    {
        $users = empresa::findOrFail($request->id);
        $users->estado = $request->estado;
        $users->save();
        
        return redirect()->back()->with('info', 'Estado actualizado');        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
