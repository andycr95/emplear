<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\datosp;
use App\ciudad;
use App\cursos;
use App\anio;

class cursosController extends Controller
{
    public function index2($id)
    {

        $datos = datosp::find($id);
        $ciudad = ciudad::all();
        $cursos = cursos::all();
        $anio = anio::all();
        return view('empleados/cv', compact('datos','cursos','anio', 'ciudad'));
        
    }

    

    public function store(Request $request, $id)
    {   
        if (!$request->ajax()) return redirect('/');
        $cursos = new cursos(); 
        $cursos->nombre = $request->nombre;     
        $cursos->institucion = $request->institucion; 
        $cursos->horas = $request->horas; 
        $cursos->users_id = $datos->id; 
        $cursos->habilidades = $request->habilidades; 
        $cursos->ciudad_id = $request->ciudad_id; 
        $cursos->anio_id = $request->anio_id; 
        $cursos->estado = 'activo';
        $cursos->save();
        return $request->all();
    }
}
