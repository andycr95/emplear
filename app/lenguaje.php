<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class lenguaje extends Model
{
    protected $table = 'lenguaje';
    protected $fillable = ['nombre', 'estado'];

    public function vacantes()
    {
        return $this->hasMany(vacantes::class);
    }
}
