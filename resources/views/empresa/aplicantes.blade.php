@extends('empresa.principal')
    @section('header')
        <header class="site-header size-lg text-center" style="background-image: url(/img/candidatos.jpg)">
            <div class="container page-name">
                <h1 class="text-center">Aplicantes</h1>
                <p class="lead text-center">Estos son los candidatos que que han aplicado a tus ofertas</p>
                <br><br><br>
            </div>
        </header>
    @endsection
@if ($api != '')
    @if ($aplicacion != '') 
        @section('content')
            <section class="no-padding-top bg-alt">
                <div class="container">
                    <div class="row">
                    <div class="col-xs-12">
                        @foreach($aplicacion as $aplicacion)
                        @if ( $aplicacion->users->datosp != '' )
                        <h2 class="control-label"></h2>
                        <div class="row grid-jobs">
                            <div class="col-md-4 grid-item job">
                            <a class="item-block" href="/candidato/perfil/{{$aplicacion->users->id}}">
                                    <header>
                                        @if ( $aplicacion->users->datosp->foto != '' )
                                        <img src="{{$aplicacion->users->datosp->foto}}" alt="">     
                                        @else
                                        <img src="/img/avatar.jpg" alt="">  
                                        @endif
                                        <div class="hgroup">
                                            <h4>{{$aplicacion->users->name}} {{$aplicacion->users->apellidos}}</h4>
                                            <h5>{{$aplicacion->users->datosp->areas->nombre}}</h5>
                                            <span class="label label-success"></span>
                                        </div>
                                    </header>
                                    <div class="item-body">
                                        <ul class="details">
                                            <li><i class="fa fa-map-marker"></i><span>{{$aplicacion->users->datosp->ciudad->nombre}} - {{$aplicacion->users->datosp->ciudad->departamento->nombre}}</span></li>
                                        </ul>
                                    </div>
                                </a>
                            </div>
                        </div>
                        @else 
                        @endif
                        @endforeach
                    </div>
                </div>
                </div>
            </section>
        @endsection
    @elseif($aplicacion == '') 
        @section('content')
            <section class="no-padding-top bg-alt">
                <h1 class="text-center">Estamos procesando las aplicaciones a sus ofertas</h1>
            </section>
        @endsection
    @endif
@else
    @section('content')
        <section class="no-padding-top bg-alt">
            <h1 class="text-center">Nadie ah aplicado a tus ofertas</h1>
        </section>
    @endsection
@endif