<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class genero extends Model
{
    protected $table = 'genero';
    protected $fillable = ['nombre', 'estado'];

    public function datosp()
    {
        return $this->hasMany(datosp::class);
    }
}
